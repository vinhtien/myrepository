USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_InsertUpdateJudgement', 'P') IS NOT NULL
    DROP PROC dbo.sp_InsertUpdateJudgement
GO

CREATE PROCEDURE dbo.sp_InsertUpdateJudgement
(
    @PI_ClaimId             INT,
    @PI_JudgementStatus     INT,
    @PI_JudgementType       INT,
    @PI_JudgementContent    NVARCHAR(1000),
    @PI_BuyerRefundAmount   DECIMAL(18,4),
    @PI_SellerRefundAmount  DECIMAL(18,4),
    @PI_IsSellerViolation   BIT,
    @PI_User                VARCHAR(50),
    @PO_ErrorMessage        NVARCHAR(100) OUTPUT
)
AS
BEGIN
    SET @PO_ErrorMessage = '';
    DECLARE @CurrentDate AS DATETIME = GETDATE();
    DECLARE @RowCount AS INT = -1;

    BEGIN TRY
        MERGE ClaimJudgement AS CJ
        USING
            ( SELECT
                @PI_ClaimId             AS ClaimId,
                @PI_JudgementStatus     AS JudgementStatus,
                @CurrentDate            AS JudgementStatusDate,
                @PI_JudgementType       AS JudgementType,
                @PI_JudgementContent    AS JudgementContent,
                @PI_BuyerRefundAmount   AS BuyerRefundAmount,
                @PI_SellerRefundAmount  AS SellerRefundAmount,
                @PI_IsSellerViolation   AS IsSellerViolation,
                0                       AS IsDeleted,
                @PI_User                AS CreatedUser,
                @CurrentDate            AS CreatedDate,
                @PI_User                AS UpdatedUser,
                @CurrentDate            AS UpdatedDate
            ) AS PCJ
        ON CJ.ClaimId = PCJ.ClaimId AND CJ.IsDeleted = 0
        WHEN MATCHED THEN
            UPDATE SET
                CJ.JudgementType = PCJ.JudgementType,
                CJ.JudgementContent = PCJ.JudgementContent,
                CJ.BuyerRefundAmount = PCJ.BuyerRefundAmount,
                CJ.SellerRefundAmount = PCJ.SellerRefundAmount,
                CJ.IsSellerViolation = PCJ.IsSellerViolation,
                CJ.UpdatedUser = PCJ.UpdatedUser,
                CJ.UpdatedDate = PCJ.UpdatedDate
        WHEN NOT MATCHED BY TARGET THEN
            INSERT
            (
                ClaimId,
                JudgementStatus,
                JudgementStatusDate,
                JudgementType,
                JudgementContent,
                BuyerRefundAmount,
                SellerRefundAmount,
                IsSellerViolation,                
                IsDeleted,
                CreatedUser,
                CreatedDate,
                UpdatedUser,
                UpdatedDate
            )
            VALUES
            (
                PCJ.ClaimId,
                PCJ.JudgementStatus,
                PCJ.JudgementStatusDate,
                PCJ.JudgementType,
                PCJ.JudgementContent,
                PCJ.BuyerRefundAmount,
                PCJ.SellerRefundAmount,
                PCJ.IsSellerViolation,
                PCJ.IsDeleted,
                PCJ.CreatedUser,
                PCJ.CreatedDate,
                PCJ.UpdatedUser,
                PCJ.UpdatedDate
            );

        SET @RowCount = @@ROWCOUNT;
        RETURN @RowCount;
    END TRY
    
    BEGIN CATCH
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_InsertUpdateJudgement!';
        RETURN @RowCount;
    END CATCH
END
GO