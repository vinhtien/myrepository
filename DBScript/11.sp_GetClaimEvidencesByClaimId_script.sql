USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_GetClaimEvidencesByClaimId', 'P') IS NOT NULL
    DROP PROC dbo.sp_GetClaimEvidencesByClaimId
GO

CREATE PROCEDURE dbo.sp_GetClaimEvidencesByClaimId
(
    @PI_ClaimId  INT,
    @PO_ErrorMessage  NVARCHAR(100) OUTPUT  
)
AS
BEGIN
    SET @PO_ErrorMessage = '';

    BEGIN TRY
        SELECT
            ClaimId,
            DocumentUrl,
            DocumentName,
            DisplayOrder
        FROM
            ClaimEvidence
        WHERE
            ClaimId = @PI_ClaimId
        AND IsDeleted = 0
    END TRY
    
    BEGIN CATCH
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_GetClaimEvidencesByClaimId!';
    END CATCH
END
GO