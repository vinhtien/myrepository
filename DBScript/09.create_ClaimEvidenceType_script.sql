USE [ApiBmc]
GO

/* Create a user-defined table type */
CREATE TYPE ClaimEvidenceType AS TABLE 
(
    [ClaimId] [INT],
    [DocumentUrl] [NVARCHAR](255),
    [DocumentName] [NVARCHAR](255),
    [DisplayOrder] [INT]
)
GO