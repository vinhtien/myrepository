USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_UpdateClaimEvidences', 'P') IS NOT NULL
    DROP PROC dbo.sp_UpdateClaimEvidences
GO

CREATE PROCEDURE dbo.sp_UpdateClaimEvidences
(
    @PI_ClaimId           INT,
    @PI_ClaimEvidences    ClaimEvidenceType READONLY,
    @PI_User              VARCHAR(50),
    @PO_ErrorMessage      NVARCHAR(100) OUTPUT
)
AS
BEGIN
    SET @PO_ErrorMessage = '';
    DECLARE @CurrentDate AS DATETIME = GETDATE();
    DECLARE @RowCount AS INT = -1;
    
    BEGIN TRANSACTION; 

    BEGIN TRY
        IF NOT EXISTS (SELECT ClaimId FROM Claim WHERE ClaimId = @PI_ClaimId AND IsDeleted = 0)
            BEGIN
                ROLLBACK;
                SET @PO_ErrorMessage = N'Khiếu nại này không tồn tại!';
                RETURN @RowCount;
            END

        --Thực hiện bổ sung evidence
        IF EXISTS (SELECT * FROM @PI_ClaimEvidences)
            BEGIN
                MERGE ClaimEvidence AS CE
                USING
                (
                    SELECT
                        @PI_ClaimId     AS ClaimId,
                        DocumentUrl     AS DocumentUrl,
                        DocumentName    AS DocumentName,
                        DisplayOrder    AS DisplayOrder,
                        0               AS IsDeleted,
                        @PI_User        AS CreatedUser,
                        @CurrentDate    AS CreatedDate,
                        @PI_User        AS UpdatedUser,
                        @CurrentDate    AS UpdatedDate
                    FROM
                        @PI_ClaimEvidences
                ) AS PCE
                ON (CE.ClaimId = PCE.ClaimId AND CE.DisplayOrder = PCE.DisplayOrder)
                WHEN MATCHED THEN
                    UPDATE
                    SET
                        CE.DocumentUrl = PCE.DocumentUrl,
                        CE.DocumentName = PCE.DocumentName,
                        CE.UpdatedUser = PCE.UpdatedUser,
                        CE.UpdatedDate = PCE.UpdatedDate
                WHEN NOT MATCHED BY TARGET THEN
                    INSERT
                    (
                        ClaimId,
                        DocumentUrl,
                        DocumentName,
                        DisplayOrder,
                        IsDeleted,
                        CreatedUser,
                        CreatedDate,
                        UpdatedUser,
                        UpdatedDate
                    )
                    VALUES
                    (
                        PCE.ClaimId,
                        PCE.DocumentUrl,
                        PCE.DocumentName,
                        PCE.DisplayOrder,
                        PCE.IsDeleted,
                        PCE.CreatedUser,
                        PCE.CreatedDate,
                        PCE.UpdatedUser,
                        PCE.UpdatedDate
                    );
            END

        SET @RowCount = @@ROWCOUNT;
        COMMIT;
        RETURN @RowCount;
    END TRY
    
    BEGIN CATCH
        ROLLBACK;
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_UpdateClaimEvidences!';
        RETURN @RowCount;
    END CATCH
END
GO