USE [ApiBmc]
GO
IF OBJECT_ID('dbo.fnc_CreateClaimNo', 'P') IS NOT NULL
    DROP PROC dbo.fnc_CreateClaimNo
GO

CREATE FUNCTION dbo.fnc_CreateClaimNo
(
    @PI_ClaimId      INT
)
RETURNS VARCHAR(50)
AS
BEGIN
    DECLARE @RET_ClaimNo AS VARCHAR(50);
    
    SET @RET_ClaimNo = CONVERT(VARCHAR(50), @PI_ClaimId);

    WHILE LEN(@RET_ClaimNo) < 6
    BEGIN
       SET @RET_ClaimNo = '0' + @RET_ClaimNo;
    END
    
    SET @RET_ClaimNo = 'CL' + @RET_ClaimNo;
    
    RETURN @RET_ClaimNo;
END
GO