USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_UpdateClaim', 'P') IS NOT NULL
    DROP PROC dbo.sp_UpdateClaim
GO

CREATE PROCEDURE dbo.sp_UpdateClaim
(
    @PI_ClaimId             INT,
    @PI_ClaimType           INT,
    @PI_Reason              INT,
    @PI_ReasonDetail        NVARCHAR(500),
    @PI_ClaimEvidences      ClaimEvidenceType READONLY,
    @PI_User                VARCHAR(50),
    @PO_ErrorMessage        NVARCHAR(100) OUTPUT
)
AS
BEGIN
    SET @PO_ErrorMessage = '';
    DECLARE @CurrentDate AS DATETIME = GETDATE();
    DECLARE @RowCount AS INT = -1;

    BEGIN TRANSACTION; 
    
    BEGIN TRY
        --Kiểm tra khiếu nại có tồn tại hay không
        IF NOT EXISTS (SELECT ClaimId FROM Claim WHERE ClaimId = @PI_ClaimId AND IsDeleted = 0)
            BEGIN
                ROLLBACK;
                SET @PO_ErrorMessage = N'Không tồn tại khiếu nại này!';
                RETURN @RowCount;
            END

        --Thực hiện cập nhật thông tin khiếu nại
        UPDATE
            Claim
        SET
            ClaimType = @PI_ClaimType,
            Reason = @PI_Reason,
            ReasonDetail = @PI_ReasonDetail,
            UpdatedUser = @PI_User,
            UpdatedDate = @CurrentDate
        WHERE
            ClaimId = @PI_ClaimId
        AND IsDeleted = 0;

        SET @RowCount = @@ROWCOUNT;
        SET NOCOUNT ON;
        
        IF EXISTS (SELECT * FROM @PI_ClaimEvidences)
            BEGIN
                INSERT INTO ClaimEvidence
                SELECT
                    @PI_ClaimId     AS ClaimId,
                    DocumentUrl     AS DocumentUrl,
                    DocumentName    AS DocumentName,
                    99              AS DisplayOrder,
                    0               AS IsDeleted,
                    @PI_User        AS CreatedUser,
                    @CurrentDate    AS CreatedDate,
                    @PI_User        AS UpdatedUser,
                    @CurrentDate    AS UpdatedDate
                FROM
                    @PI_ClaimEvidences;
            END

        COMMIT;
        RETURN @RowCount;
    END TRY
    
    BEGIN CATCH
        ROLLBACK;
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_UpdateClaim!';
        RETURN @RowCount;
    END CATCH
END
GO