USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_UpdateJudgementStatus', 'P') IS NOT NULL
    DROP PROC dbo.sp_UpdateJudgementStatus
GO

CREATE PROCEDURE dbo.sp_UpdateJudgementStatus
(
    @PI_ClaimId             INT,
    @PI_JudgementStatus     INT,
    @PI_User                VARCHAR(50),
    @PO_ErrorMessage        NVARCHAR(100) OUTPUT
)
AS
BEGIN
    SET @PO_ErrorMessage = '';
    DECLARE @CurrentDate AS DATETIME = GETDATE();
    DECLARE @RowCount AS INT = -1;

    BEGIN TRY
        --Kiểm tra đã tồn tại phán quyết của khiếu nại chưa
        IF NOT EXISTS (SELECT ClaimId FROM ClaimJudgement WHERE ClaimId = @PI_ClaimId AND IsDeleted = 0)
            BEGIN
                SET @PO_ErrorMessage = N'Chưa có phán quyết của khiếu nại này!';
                RETURN @RowCount;
            END

        --Thực hiện update trạng thái phán quyết
        UPDATE
            ClaimJudgement
        SET
            JudgementStatus = @PI_JudgementStatus,
            JudgementStatusDate = @CurrentDate,
            UpdatedUser = @PI_User
        WHERE
            ClaimId = @PI_ClaimId
        AND IsDeleted = 0;

        SET @RowCount = @@ROWCOUNT;
        RETURN @RowCount;
    END TRY
    
    BEGIN CATCH
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_UpdateJudgementStatus!';
        RETURN @RowCount;
    END CATCH
END
GO