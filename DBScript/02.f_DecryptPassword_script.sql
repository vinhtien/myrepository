USE [ApiBmc]
GO
IF OBJECT_ID('dbo.f_DecryptPassword', 'P') IS NOT NULL
    DROP PROC dbo.f_DecryptPassword
GO

CREATE FUNCTION dbo.f_DecryptPassword
(
    @PI_Encrypted      VARBINARY(100)
)
RETURNS VARCHAR(20)
AS
BEGIN
    DECLARE @KeyMaster AS VARCHAR(10);
    SET @KeyMaster = 'Sendo';
    
    RETURN CONVERT(VARCHAR(20),DECRYPTBYPASSPHRASE(@KeyMaster, @PI_Encrypted));
END
GO