USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_UpdateJudgement', 'P') IS NOT NULL
    DROP PROC dbo.sp_UpdateJudgement
GO

CREATE PROCEDURE dbo.sp_UpdateJudgement
(
    @PI_ClaimId             INT,
    @PI_JudgementType       INT,
    @PI_JudgementStatus     INT,
    @PI_JudgementContent    NVARCHAR(1000),
    @PI_BuyerRefundAmount   DECIMAL(18,4),
    @PI_SellerRefundAmount  DECIMAL(18,4),
    @PI_IsSellerViolation   BIT,
    @PI_User                VARCHAR(50),
    @PO_ErrorMessage        NVARCHAR(100) OUTPUT
)
AS
BEGIN
    SET @PO_ErrorMessage = '';
    DECLARE @CurrentDate AS DATETIME = GETDATE();
    DECLARE @RowCount AS INT = -1;

    BEGIN TRY
        IF NOT EXISTS (SELECT ClaimId FROM ClaimJudgement WHERE ClaimId = @PI_ClaimId AND IsDeleted = 0)
            BEGIN
                SET @PO_ErrorMessage = N'Không tồn tại phán quyết của khiếu nại này!';
                RETURN @RowCount;
            END
    
        UPDATE ClaimJudgement
        SET
            JudgementType = @PI_JudgementType,
            JudgementStatus = @PI_JudgementStatus,
            JudgementStatusDate = @CurrentDate,
            JudgementContent = @PI_JudgementContent,
            BuyerRefundAmount = @PI_BuyerRefundAmount,
            SellerRefundAmount = @PI_SellerRefundAmount,
            IsSellerViolation = @PI_IsSellerViolation,
            UpdatedUser = @PI_User,
            UpdatedDate = @CurrentDate
        WHERE
            ClaimId = @PI_ClaimId
        AND IsDeleted = 0;

        SET @RowCount = @@ROWCOUNT;
        RETURN @RowCount;
    END TRY
    
    BEGIN CATCH
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_UpdateJudgement!';
        RETURN @RowCount;
    END CATCH
END
GO