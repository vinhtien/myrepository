USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_CreateClaim', 'P') IS NOT NULL
    DROP PROC dbo.sp_CreateClaim
GO

CREATE PROCEDURE dbo.sp_CreateClaim
(
    @PI_ClaimType           INT,
    @PI_Reason              INT,
    @PI_ReasonDetail        NVARCHAR(500),
    
    @PI_ClaimEvidences      ClaimEvidenceType READONLY,
    @PI_OrderId             INT,
    @PI_OrderNumber         VARCHAR(50),
    @PI_OrderDate           DATETIME,
    @PI_TotalPayment        DECIMAL(18,4),
    @PI_BuyerId             INT,
    @PI_BuyerName           NVARCHAR(100),
    @PI_BuyerPhone          VARCHAR(20),
    @PI_BuyerEmail          VARCHAR(255),
    @PI_StoreId             INT,
    @PI_StoreName           NVARCHAR(400),
    @PI_StorePhone          VARCHAR(20),
    @PI_StoreEmail          VARCHAR(255),
    @PI_CarrierName         NVARCHAR(100),
    @PI_DiscountAmount      DECIMAL(18, 4),
    @PI_ShippingFee         DECIMAL(18, 4),
    @PI_SendoSupportFee     DECIMAL(18, 4),
    @PI_SellerShippingFee   DECIMAL(18, 4),
  
    @PI_User                VARCHAR(50),
    @PO_ErrorMessage        NVARCHAR(100) OUTPUT,
    @PO_Id                  INT OUTPUT
)
AS
BEGIN
    SET @PO_ErrorMessage = '';
    DECLARE @CurrentDate AS DATETIME = GETDATE();
    DECLARE @RowCount AS INT = -1;
    DECLARE @ClaimId AS INT = CAST(IDENT_CURRENT('Claim') AS INT) + 1;
    
    IF @ClaimId = 2
    BEGIN
        IF NOT EXISTS (SELECT ClaimId FROM Claim)
            BEGIN
                SET @ClaimId = 1;
            END
    END
    
    BEGIN TRANSACTION; 

    BEGIN TRY
        IF EXISTS (SELECT ClaimId FROM Claim WHERE OrderId = @PI_OrderId AND IsDeleted = 0 AND ClaimStatus > 0)
            BEGIN
                ROLLBACK;
                SET @PO_ErrorMessage = N'Đã tồn tại khiếu nại của Order này!';
                SET @PO_Id = 0;
                RETURN @RowCount;
            END
    
        --Thực hiện Insert table Claim
        INSERT INTO Claim (
            ClaimNo,
            ClaimType,
            ClaimStatus,
            ClaimStatusDate,
            Reason,
            ReasonDetail,
            OrderId,
            OrderNumber,
            OrderDate,
            TotalPayment,
            BuyerId,
            BuyerName,
            BuyerPhone,
            BuyerEmail,
            StoreId,
            StoreName,
            StorePhone,
            StoreEmail,
            CarrierName,
            DiscountAmount,
            ShippingFee,
            SendoSupportFee,
            SellerShippingFee,
            IsDeleted,
            CreatedUser,
            CreatedDate,
            UpdatedUser,
            UpdatedDate
        ) VALUES (
            dbo.fnc_CreateClaimNo(@ClaimId),
            @PI_ClaimType,
            1,
            @CurrentDate,
            @PI_Reason,
            @PI_ReasonDetail,
            @PI_OrderId,
            @PI_OrderNumber,
            @PI_OrderDate,
            @PI_TotalPayment,
            @PI_BuyerId,
            @PI_BuyerName,
            @PI_BuyerPhone,
            @PI_BuyerEmail,
            @PI_StoreId,
            @PI_StoreName,
            @PI_StorePhone,
            @PI_StoreEmail,
            @PI_CarrierName,
            @PI_DiscountAmount,
            @PI_ShippingFee,
            @PI_SendoSupportFee,
            @PI_SellerShippingFee,
            0,
            @PI_User,
            @CurrentDate,
            @PI_User,
            @CurrentDate
        );

        SET @RowCount = @@ROWCOUNT;
        SET NOCOUNT ON;

        --Trường hợp có truyền danh sách claim evidence thì thực hiện insert table ClaimEvidence
        IF EXISTS (SELECT * FROM @PI_ClaimEvidences)
            BEGIN
                INSERT INTO ClaimEvidence
                (
                    ClaimId,
                    DocumentUrl,
                    DocumentName,
                    DisplayOrder,
                    IsDeleted,
                    CreatedUser,
                    CreatedDate,
                    UpdatedUser,
                    UpdatedDate
                )
                SELECT
                    @ClaimId,
                    DocumentUrl,
                    DocumentName,
                    DisplayOrder,
                    0,
                    @PI_User,
                    @CurrentDate,
                    @PI_User,
                    @CurrentDate
                FROM
                    @PI_ClaimEvidences;
            END

        SET @PO_Id = @ClaimId;
        COMMIT;
        RETURN @RowCount;
    END TRY
    
    BEGIN CATCH
        ROLLBACK;
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_CreateClaim!';
        SET @PO_Id = 0;
        RETURN @RowCount;
    END CATCH
END
GO