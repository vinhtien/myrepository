USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_InsertJudgement', 'P') IS NOT NULL
    DROP PROC dbo.sp_InsertJudgement
GO

CREATE PROCEDURE dbo.sp_InsertJudgement
(
    @PI_ClaimId             INT,
    @PI_JudgementStatus     INT,
    @PI_JudgementType       INT,
    @PI_JudgementContent    NVARCHAR(1000),
    @PI_BuyerRefundAmount   DECIMAL(18,4),
    @PI_SellerRefundAmount  DECIMAL(18,4),
    @PI_IsSellerViolation   BIT,
    @PI_User                VARCHAR(50),
    @PO_ErrorMessage        NVARCHAR(100) OUTPUT
)
AS
BEGIN
    SET @PO_ErrorMessage = '';
    DECLARE @CurrentDate AS DATETIME = GETDATE();
    DECLARE @RowCount AS INT = -1;

    BEGIN TRY
        IF EXISTS (SELECT ClaimId FROM ClaimJudgement WHERE ClaimId = @PI_ClaimId AND IsDeleted = 0)
            BEGIN
                SET @PO_ErrorMessage = N'Đã tồn tại phán quyết của khiếu nại này!';
                RETURN @RowCount;
            END
    
        INSERT INTO ClaimJudgement
        (
            ClaimId,
            JudgementStatus,
            JudgementStatusDate,
            JudgementType,
            JudgementContent,
            BuyerRefundAmount,
            SellerRefundAmount,
            IsSellerViolation,                
            IsDeleted,
            CreatedUser,
            CreatedDate,
            UpdatedUser,
            UpdatedDate
        )
        SELECT
            @PI_ClaimId,
            @PI_JudgementStatus,
            @CurrentDate,
            @PI_JudgementType,
            @PI_JudgementContent,
            @PI_BuyerRefundAmount,
            @PI_SellerRefundAmount,
            @PI_IsSellerViolation,
            0,
            @PI_User,
            @CurrentDate,
            @PI_User,
            @CurrentDate;

        SET @RowCount = @@ROWCOUNT;
        RETURN @RowCount;
    END TRY
    
    BEGIN CATCH
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_InsertJudgement!';
        RETURN @RowCount;
    END CATCH
END
GO