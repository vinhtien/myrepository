USE [ApiBmc]
GO

/****** Object:  Table [dbo].[Country] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Country](
    [CountryId] [int] IDENTITY(1,1) NOT NULL,
    [Code] [varchar](10) NOT NULL,
    [CountryName] [nvarchar](50) NULL,
    [AllowsBilling] [bit] NOT NULL,
    [AllowsShipping] [bit] NOT NULL,
    [TwoLetterIsoCode] [varchar](2) NULL,
    [ThreeLetterIsoCode] [varchar](3) NULL,
    [NumericIsoCode] [int] NULL,
    [Published] [bit] NOT NULL,
    [DisplayOrder] [int] NOT NULL, 
    [IsDeleted] [bit] NOT NULL,
    [CreatedUser] [varchar](50) NULL,
    [CreatedDate] [datetime] NULL,
    [UpdatedUser] [varchar](50) NULL,
    [UpdatedDate] [datetime] NULL,

    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([CountryId] ASC)
    WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_AllowsBilling]  DEFAULT ((0)) FOR [AllowsBilling]
GO
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_AllowsShipping]  DEFAULT ((0)) FOR [AllowsShipping]
GO
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_Published]  DEFAULT ((1)) FOR [Published]
GO
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_DisplayOrder]  DEFAULT ((0)) FOR [DisplayOrder]
GO
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO

SET ANSI_PADDING OFF
GO

--Insert Data Setting Common
DECLARE @CurrentDate AS DATETIME = GETDATE();
INSERT INTO [dbo].[Country](Code,CountryName,TwoLetterIsoCode,ThreeLetterIsoCode,NumericIsoCode,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate)
VALUES ('VN',N'Việt Nam','VN','VNM',704,'admin',@CurrentDate,'admin',@CurrentDate);

