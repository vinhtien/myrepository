USE [ApiBmc]
GO

/****** Object:  Table [dbo].[Region] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Region](
    [RegionId] [int] NOT NULL,
    [CountryCode] [varchar](10) NOT NULL,
    [RegionName] [nvarchar](100) NULL,
    [Abbreviation] [varchar](50) NULL,
    [Published] [bit] NOT NULL,
    [DisplayOrder] [int] NOT NULL,
    [IsDeleted] [bit] NOT NULL,
    [CreatedUser] [varchar](50) NULL,
    [CreatedDate] [datetime] NULL,
    [UpdatedUser] [varchar](50) NULL,
    [UpdatedDate] [datetime] NULL,

    CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED ([RegionId] ASC)
    WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Region] ADD  CONSTRAINT [DF_Region_Published]  DEFAULT ((1)) FOR [Published]
GO
ALTER TABLE [dbo].[Region] ADD  CONSTRAINT [DF_Region_DisplayOrder]  DEFAULT ((0)) FOR [DisplayOrder]
GO
ALTER TABLE [dbo].[Region] ADD  CONSTRAINT [DF_Region_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO

SET ANSI_PADDING OFF
GO

--Insert Data Setting Common
DECLARE @CurrentDate AS DATETIME = GETDATE();
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (1,'VN',N'Hồ Chí Minh','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (2,'VN',N'Hà Nội','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (3,'VN',N'An Giang','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (5,'VN',N'Bắc Kạn','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (6,'VN',N'Bắc Giang','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (7,'VN',N'Bạc Liêu','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (8,'VN',N'Bắc Ninh','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (9,'VN',N'Bến Tre','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (11,'VN',N'Bình Định','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (12,'VN',N'Bình Phước','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (13,'VN',N'Bình Thuận','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (14,'VN',N'Cà Mau','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (15,'VN',N'Cần Thơ','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (16,'VN',N'Cao Bằng','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (17,'VN',N'Đà Nẵng','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (18,'VN',N'Đắk Lắk','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (19,'VN',N'Điện Biên','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (20,'VN',N'Đồng Nai','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (21,'VN',N'Gia Lai','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (22,'VN',N'Hà Giang','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (23,'VN',N'Hà Nam','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (24,'VN',N'Hậu Giang','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (25,'VN',N'Hà Tĩnh','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (26,'VN',N'Hải Dương','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (27,'VN',N'Hải Phòng','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (28,'VN',N'Hòa Bình','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (30,'VN',N'Khánh Hòa','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (32,'VN',N'Lai Châu','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (33,'VN',N'Lâm Đồng','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (34,'VN',N'Lạng Sơn','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (35,'VN',N'Lào Cai','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (36,'VN',N'Long An','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (37,'VN',N'Nam Định','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (38,'VN',N'Nghệ An','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (39,'VN',N'Ninh Thuận','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (40,'VN',N'Phú Thọ','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (41,'VN',N'Phú Yên','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (42,'VN',N'Quảng Bình','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (43,'VN',N'Quảng Nam','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (44,'VN',N'Quảng Ngãi','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (45,'VN',N'Quảng Ninh','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (46,'VN',N'Quảng Trị','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (47,'VN',N'Sóc Trăng','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (48,'VN',N'Tây Ninh','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (49,'VN',N'Thái Bình','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (50,'VN',N'Thái Nguyên','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (51,'VN',N'Thanh Hóa','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (52,'VN',N'Huế','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (53,'VN',N'Tiền Giang','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (54,'VN',N'Trà Vinh','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (55,'VN',N'Tuyên Quang','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (56,'VN',N'Kiên Giang','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (57,'VN',N'Vĩnh Phúc','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (58,'VN',N'Bà Rịa–Vũng Tàu','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (59,'VN',N'Yên Bái','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (60,'VN',N'Vĩnh Long','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (61,'VN',N'Bình Dương','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (62,'VN',N'Đắk Nông','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (63,'VN',N'Đồng Tháp','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (64,'VN',N'Hưng Yên','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (65,'VN',N'Kon Tum','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (66,'VN',N'Ninh Bình','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO [dbo].[Region](RegionId,CountryCode,RegionName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (67,'VN',N'Sơn La','admin',@CurrentDate,'admin',@CurrentDate);
