USE [ApiBmc]
GO

/****** Object:  Table [dbo].[Claim] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Claim](
    [ClaimId] [int] IDENTITY(1,1) NOT NULL,
    [ClaimNo] [varchar](50) NOT NULL,
    [ClaimStatus] [int] NOT NULL,
    [Reason] [int] NOT NULL,
    [ReasonDetail] [nvarchar](500) NOT NULL,
    [TotalPayment] [decimal](18,4) NULL,
    [OrderId] [int] NOT NULL,
    [OrderNumber] [varchar](50) NULL,
    [BuyerId] [int] NULL,
    [BuyerName] [nvarchar](100) NULL,
    [StoreId] [int] NULL,
    [StoreName] [nvarchar](400) NULL,
    [SellerNote] [nvarchar](500) NULL,
    [CSNote] [nvarchar](500) NULL,
    [IsDeleted] [bit] NOT NULL,
    [CreatedUser] [varchar](50) NULL,
    [CreatedDate] [datetime] NULL,
    [UpdatedUser] [varchar](50) NULL,
    [UpdatedDate] [datetime] NULL,

    CONSTRAINT [PK_Claim] PRIMARY KEY CLUSTERED ([ClaimId] ASC)
    WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Claim] ADD  CONSTRAINT [DF_Claim_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO

SET ANSI_PADDING OFF
GO
