USE [ApiBmc]
GO
DECLARE @CurrentDate AS DATETIME = GETDATE();

INSERT INTO Users(UserName,Password,Email,FullName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES ('admin',dbo.f_EncryptPassword('123456'),'',N'Administrator','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO Users(UserName,Password,Email,FullName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES ('tienhv',dbo.f_EncryptPassword('123456'),'tienhv3@sendo.vn',N'Huỳnh Vĩnh Tiến','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO Users(UserName,Password,Email,FullName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES ('nganlt',dbo.f_EncryptPassword('123456'),'ngantl@sendo.vn',N'Tân Lâm Ngân','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO Users(UserName,Password,Email,FullName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES ('uypc',dbo.f_EncryptPassword('123456'),'uypc@sendo.vn',N'Phan Công Úy','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO Users(UserName,Password,Email,FullName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES ('dunghc',dbo.f_EncryptPassword('123456'),'dunghc@sendo.vn',N'Huỳnh Công Dũng','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO Users(UserName,Password,Email,FullName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES ('hiepln',dbo.f_EncryptPassword('123456'),'hiepln@sendo.vn',N'Lưu Nghĩa Hiệp','admin',@CurrentDate,'admin',@CurrentDate);

INSERT INTO Roles(RoleName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (N'Admin','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO Roles(RoleName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (N'CS','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO Roles(RoleName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (N'Buyer','admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO Roles(RoleName,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (N'Seller','admin',@CurrentDate,'admin',@CurrentDate);

INSERT INTO UserRole(UserId,RoleId,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (1,1,'admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO UserRole(UserId,RoleId,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (2,1,'admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO UserRole(UserId,RoleId,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (3,1,'admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO UserRole(UserId,RoleId,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (4,1,'admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO UserRole(UserId,RoleId,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (5,1,'admin',@CurrentDate,'admin',@CurrentDate);
INSERT INTO UserRole(UserId,RoleId,CreatedUser,CreatedDate,UpdatedUser,UpdatedDate) VALUES (6,1,'admin',@CurrentDate,'admin',@CurrentDate);