USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_GetClaimListByBuyerId_TabProcessing', 'P') IS NOT NULL
    DROP PROC dbo.sp_GetClaimListByBuyerId_TabProcessing
GO

CREATE PROCEDURE dbo.sp_GetClaimListByBuyerId_TabProcessing
(
    @PI_BuyerId       INT,
    @PO_ErrorMessage  NVARCHAR(100) OUTPUT  
)
AS
BEGIN
    SET @PO_ErrorMessage = '';

    BEGIN TRY
        SELECT
            C.ClaimId,
            C.ClaimNo,
            C.ClaimStatus,
            C.ClaimStatusDate,
            C.ClaimType,
            C.Reason,
            C.ReasonDetail,
            C.OrderId,
            C.OrderNumber,
            C.OrderDate,
            C.TotalPayment,
            C.BuyerId,
            C.BuyerName,
            C.BuyerPhone,
            C.BuyerEmail,
            C.StoreId,
            C.StoreName,
            C.StorePhone,
            C.StoreEmail,
            C.CarrierName,
            C.DiscountAmount,
            C.ShippingFee,
            C.SendoSupportFee,
            C.SellerShippingFee,
            C.BuyerNote,
            C.SellerNote,
            C.CSNote,
            C.CreatedDate,
            C.UpdatedUser,
            C.UpdatedDate
        FROM
            Claim C
        WHERE
            C.BuyerId = @PI_BuyerId
        AND C.IsDeleted = 0
        AND C.ClaimStatus BETWEEN 1 AND 9

    END TRY
    
    BEGIN CATCH
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_GetClaimListByBuyerId_TabProcessing!';
    END CATCH
END
GO