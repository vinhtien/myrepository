USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_InsertComment', 'P') IS NOT NULL
    DROP PROC dbo.sp_InsertComment
GO

CREATE PROCEDURE dbo.sp_InsertComment
(
    @PI_ClaimId         INT,
    @PI_Comment         NVARCHAR(255),
    @PI_User            VARCHAR(50),
    @PO_ErrorMessage    NVARCHAR(100) OUTPUT
)
AS
BEGIN
    SET @PO_ErrorMessage = '';
    DECLARE @CurrentDate AS DATETIME = GETDATE();
    DECLARE @RowCount AS INT = -1;

    BEGIN TRY
        INSERT INTO ClaimComment
        (
            ClaimId,
            Comment,               
            IsDeleted,
            CreatedUser,
            CreatedDate,
            UpdatedUser,
            UpdatedDate
        )
        VALUES
        (
            @PI_ClaimId,
            @PI_Comment,
            0,
            @PI_User,
            @CurrentDate,
            @PI_User,
            @CurrentDate
        );

        SET @RowCount = @@ROWCOUNT;
        RETURN @RowCount;
    END TRY
    
    BEGIN CATCH
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_InsertComment!';
        RETURN @RowCount;
    END CATCH
END
GO