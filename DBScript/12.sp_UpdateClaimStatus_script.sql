USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_UpdateClaimStatus', 'P') IS NOT NULL
    DROP PROC dbo.sp_UpdateClaimStatus
GO

CREATE PROCEDURE dbo.sp_UpdateClaimStatus
(
    @PI_ClaimId         INT,
    @PI_ClaimStatus     INT,
    @PI_User            VARCHAR(50),
    @PO_ErrorMessage    NVARCHAR(100) OUTPUT
)
AS
BEGIN
    SET @PO_ErrorMessage = '';
    DECLARE @CurrentDate AS DATETIME = GETDATE();
    DECLARE @RowCount AS INT = -1;

    BEGIN TRY
        --Kiểm tra khiếu nại có tồn tại hay không
        IF NOT EXISTS (SELECT ClaimId FROM Claim WHERE ClaimId = @PI_ClaimId AND IsDeleted = 0)
            BEGIN
                SET @PO_ErrorMessage = N'Không tồn tại khiếu nại này!';
                RETURN @RowCount;
            END

        --Kiểm tra trạng thái khiếu nại có hợp lệ hay không
        IF @PI_ClaimStatus <= 1 OR @PI_ClaimStatus > 12
            BEGIN
                SET @PO_ErrorMessage = N'Trạng thái khiếu nại không hợp lệ!';
                RETURN @RowCount;
            END

        --Thực hiện cập nhật trạng thái khiếu nại
        UPDATE
            Claim
        SET
            ClaimStatus = @PI_ClaimStatus,
            ClaimStatusDate = @CurrentDate,
            UpdatedUser = @PI_User
        WHERE
            ClaimId = @PI_ClaimId
        AND IsDeleted = 0;

        SET @RowCount = @@ROWCOUNT;
        RETURN @RowCount;
    END TRY
    
    BEGIN CATCH
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_UpdateClaimStatus!';
        RETURN @RowCount;
    END CATCH
END
GO