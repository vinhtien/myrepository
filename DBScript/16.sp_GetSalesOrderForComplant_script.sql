USE [InsideTest2]
GO
IF OBJECT_ID('dbo.sp_GetSalesOrderForComplaint', 'P') IS NOT NULL
    DROP PROC dbo.sp_GetSalesOrderForComplaint
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================
-- Author:          Tienhv3
-- Create date:     20-06-2017
-- Description:     Get danh sách order theo buyer và trạng thái
-- ==============================================================
CREATE PROCEDURE [dbo].[sp_GetSalesOrderForComplaint]
( 
    @PI_ComplaintStatus    INT,                     -- Trạng thái(1: có thể thực hiện khiếu nại,2: đã khiếu nại,3: hoàn tất)
    @PI_BuyerId            INT,                     -- Id của buyer
    @PO_TodoTotal          INT OUTPUT,              -- Tổng số đơn hàng có thể thực hiện khiếu nại của buyer (không quá 48h)
    @PO_DoingTotal         INT OUTPUT,              -- Tổng số đơn hàng đã khiếu nại của buyer (không quá 48h)
    @PO_CompleteTotal      INT OUTPUT,              -- Tổng số đơn hàng hoàn tất của buyer
    @PO_ErrorMessage       NVARCHAR(100) OUTPUT     -- Message thông báo lỗi
)
AS
BEGIN
    SET NOCOUNT ON;
    SET @PO_ErrorMessage = '';
    
    BEGIN TRY

        --Lấy tổng số đơn hàng chưa có khiếu nại của buyer 
        -------------------------------------------------------------------------------------
        SELECT
            @PO_TodoTotal = COUNT(*)
        FROM
            (
                SELECT
                    so.*,
                    sod.*
                FROM
                    (
                        SELECT
                            o.Id                AS Id,
                            o.OrderNumber       AS OrderNumber, 
                            o.OrderDate         AS OrderDate,
                            o.TotalAmount       AS TotalAmount,
                            o.ReceiverName      AS ReceiverName,
                            cus.Email           AS BuyerEmail,
                            st.Name             AS StoreName,
                            st.SystemURL        AS StoreUrl
                        FROM
                            SalesOrder o,
                            Store st,
                            Customer cus
                        WHERE
                            o.StoreId         = st.Id
                        AND o.BuyerId         = cus.Id
                        AND o.BuyerId         = @PI_BuyerId
                        AND o.IsDeleted       = 0
                        AND o.IsDisputing     = 0                           -- 0: không có khiếu nại
                        AND o.OrderStatus     = 7                           -- 7: trạng thái đơn hàng là đã giao hàng
                        AND o.OrderStatusDate >= DATEADD(day,-2,GETDATE())  -- không quá 48h
                    ) so
                    CROSS APPLY 
                    ( 
                        SELECT TOP 1
                            od.ProductName      AS ProductName,
                            p.PictureURL        AS PictureUrl
                        FROM
                            SalesOrderDetail od,
                            ProductVariant pv,
                            Picture p
                        WHERE
                            od.ProductVariantId = pv.Id
                        AND od.SalesOrderId     = so.Id
                        AND pv.PictureId        = p.Id
                    ) sod
            ) AS dat
        -------------------------------------------------------------------------------------
        
        --Lấy tổng số đơn hàng có khiếu nại của buyer
        -------------------------------------------------------------------------------------
        SELECT
            @PO_DoingTotal = COUNT(*)
        FROM
            (
                SELECT
                    so.*,
                    sod.*
                FROM
                    (
                        SELECT
                            o.Id                AS Id,
                            o.OrderNumber       AS OrderNumber, 
                            o.OrderDate         AS OrderDate,
                            o.TotalAmount       AS TotalAmount,
                            o.ReceiverName      AS ReceiverName,
                            cus.Email           AS BuyerEmail,
                            st.Name             AS StoreName,
                            st.SystemURL        AS StoreUrl
                        FROM
                            SalesOrder o,
                            Store st,
                            Customer cus
                        WHERE
                            o.StoreId         = st.Id
                        AND o.BuyerId         = cus.Id
                        AND o.BuyerId         = @PI_BuyerId
                        AND o.IsDeleted       = 0
                        AND o.IsDisputing     = 1                           -- 1: có khiếu nại
                        AND o.OrderStatus     = 7                           -- 7: trạng thái đơn hàng là đã giao hàng
                        AND o.OrderStatusDate >= DATEADD(day,-2,GETDATE())  -- không quá 48h
                    ) so
                    CROSS APPLY 
                    ( 
                        SELECT TOP 1
                            od.ProductName      AS ProductName,
                            p.PictureURL        AS PictureUrl
                        FROM
                            SalesOrderDetail od,
                            ProductVariant pv,
                            Picture p
                        WHERE
                            od.ProductVariantId = pv.Id
                        AND od.SalesOrderId     = so.Id
                        AND pv.PictureId        = p.Id
                    ) sod
            ) AS dat
        -------------------------------------------------------------------------------------
        
        --Lấy tổng số đơn hàng hoàn tất của buyer
        -------------------------------------------------------------------------------------
        SELECT
            @PO_CompleteTotal = COUNT(*)
        FROM
            (
                SELECT
                    so.*,
                    sod.*
                FROM
                    (
                        SELECT
                            o.Id                AS Id,
                            o.OrderNumber       AS OrderNumber, 
                            o.OrderDate         AS OrderDate,
                            o.TotalAmount       AS TotalAmount,
                            o.ReceiverName      AS ReceiverName,
                            cus.Email           AS BuyerEmail,
                            st.Name             AS StoreName,
                            st.SystemURL        AS StoreUrl
                        FROM
                            SalesOrder o,
                            Store st,
                            Customer cus
                        WHERE
                            o.StoreId         = st.Id
                        AND o.BuyerId         = cus.Id
                        AND o.BuyerId         = @PI_BuyerId
                        AND o.IsDeleted       = 0
                        AND o.IsDisputing     = 0    -- 0: không có khiếu nại
                        AND o.OrderStatus     = 8    -- 8: trạng thái đơn hàng là hoàn tất
                    ) so
                    CROSS APPLY 
                    ( 
                        SELECT TOP 1
                            od.ProductName      AS ProductName,
                            p.PictureURL        AS PictureUrl
                        FROM
                            SalesOrderDetail od,
                            ProductVariant pv,
                            Picture p
                        WHERE
                            od.ProductVariantId = pv.Id
                        AND od.SalesOrderId     = so.Id
                        AND pv.PictureId        = p.Id
                    ) sod
            ) AS dat
        -------------------------------------------------------------------------------------
        
        IF @PI_ComplaintStatus = 1
            BEGIN
                SELECT
                    so.*,
                    sod.*
                FROM
                    (
                        SELECT
                            o.Id                AS Id,
                            o.OrderNumber       AS OrderNumber, 
                            o.OrderDate         AS OrderDate,
                            o.TotalAmount       AS TotalAmount,
                            o.ReceiverName      AS ReceiverName,
                            cus.Email           AS BuyerEmail,
                            st.Name             AS StoreName,
                            st.SystemURL        AS StoreUrl
                        FROM
                            SalesOrder o,
                            Store st,
                            Customer cus
                        WHERE
                            o.StoreId         = st.Id
                        AND o.BuyerId         = cus.Id
                        AND o.BuyerId         = @PI_BuyerId
                        AND o.IsDeleted       = 0
                        AND o.IsDisputing     = 0                           -- 0: không có khiếu nại
                        AND o.OrderStatus     = 7                           -- 7: trạng thái đơn hàng là đã giao hàng
                        AND o.OrderStatusDate >= DATEADD(day,-2,GETDATE())  -- không quá 48h
                    ) so
                    CROSS APPLY 
                    ( 
                        SELECT TOP 1
                            od.ProductName      AS ProductName,
                            p.PictureURL        AS PictureUrl
                        FROM
                            SalesOrderDetail od,
                            ProductVariant pv,
                            Picture p
                        WHERE
                            od.ProductVariantId = pv.Id
                        AND od.SalesOrderId     = so.Id
                        AND pv.PictureId        = p.Id
                    ) sod
            END
        ELSE IF @PI_ComplaintStatus = 2
            BEGIN
                SELECT
                    so.*,
                    sod.*
                FROM
                    (
                        SELECT
                            o.Id                AS Id,
                            o.OrderNumber       AS OrderNumber, 
                            o.OrderDate         AS OrderDate,
                            o.TotalAmount       AS TotalAmount,
                            o.ReceiverName      AS ReceiverName,
                            cus.Email           AS BuyerEmail,
                            st.Name             AS StoreName,
                            st.SystemURL        AS StoreUrl
                        FROM
                            SalesOrder o,
                            Store st,
                            Customer cus
                        WHERE
                            o.StoreId         = st.Id
                        AND o.BuyerId         = cus.Id
                        AND o.BuyerId         = @PI_BuyerId
                        AND o.IsDeleted       = 0
                        AND o.IsDisputing     = 1                           -- 1: có khiếu nại
                        AND o.OrderStatus     = 7                           -- 7: trạng thái đơn hàng là đã giao hàng
                        AND o.OrderStatusDate >= DATEADD(day,-2,GETDATE())  -- không quá 48h
                    ) so
                    CROSS APPLY 
                    ( 
                        SELECT TOP 1
                            od.ProductName      AS ProductName,
                            p.PictureURL        AS PictureUrl
                        FROM
                            SalesOrderDetail od,
                            ProductVariant pv,
                            Picture p
                        WHERE
                            od.ProductVariantId = pv.Id
                        AND od.SalesOrderId     = so.Id
                        AND pv.PictureId        = p.Id
                    ) sod
            END
        ELSE IF @PI_ComplaintStatus = 3
            BEGIN
                SELECT
                    so.*,
                    sod.*
                FROM
                    (
                        SELECT
                            o.Id                AS Id,
                            o.OrderNumber       AS OrderNumber, 
                            o.OrderDate         AS OrderDate,
                            o.TotalAmount       AS TotalAmount,
                            o.ReceiverName      AS ReceiverName,
                            cus.Email           AS BuyerEmail,
                            st.Name             AS StoreName,
                            st.SystemURL        AS StoreUrl
                        FROM
                            SalesOrder o,
                            Store st,
                            Customer cus
                        WHERE
                            o.StoreId         = st.Id
                        AND o.BuyerId         = cus.Id
                        AND o.BuyerId         = @PI_BuyerId
                        AND o.IsDeleted       = 0
                        AND o.IsDisputing     = 0    -- 0: không có khiếu nại
                        AND o.OrderStatus     = 8    -- 8: trạng thái đơn hàng là hoàn tất
                    ) so
                    CROSS APPLY 
                    ( 
                        SELECT TOP 1
                            od.ProductName      AS ProductName,
                            p.PictureURL        AS PictureUrl
                        FROM
                            SalesOrderDetail od,
                            ProductVariant pv,
                            Picture p
                        WHERE
                            od.ProductVariantId = pv.Id
                        AND od.SalesOrderId     = so.Id
                        AND pv.PictureId        = p.Id
                    ) sod
            END
    END TRY
    
    BEGIN CATCH
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_GetSalesOrderForComplaint!';
    END CATCH
END
