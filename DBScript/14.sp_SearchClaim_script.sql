USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_SearchClaim', 'P') IS NOT NULL
    DROP PROC dbo.sp_SearchClaim
GO

CREATE PROCEDURE dbo.sp_SearchClaim
(
    @PI_ClaimNo                 VARCHAR(50),
    @PI_BuyerName               NVARCHAR(100),
    @PI_ClaimCreatedDateFrom    DATETIME,
    @PI_ClaimCreatedDateTo      DATETIME,
    @PI_ClaimStatus             INT,
    @PI_ClaimUpdatedDateFrom    DATETIME,
    @PI_ClaimUpdatedDateTo      DATETIME,
    @PI_UpdatedUser             VARCHAR(50),
    @PI_OrderNumber             VARCHAR(50),
    @PI_PageNo                  INT,
    @PI_PageSize                INT,
    @PO_TotalCount              INT OUTPUT,
    @PO_ErrorMessage            NVARCHAR(100) OUTPUT  
)
AS
BEGIN
    SET @PO_ErrorMessage = '';

    BEGIN TRY
        SELECT
            @PO_TotalCount = COUNT(*)
        FROM
            Claim
        WHERE
            (@PI_ClaimNo IS NULL OR ClaimNo LIKE '%'+@PI_ClaimNo+'%')
        AND (@PI_BuyerName IS NULL OR BuyerName LIKE N'%'+@PI_BuyerName+'%')
        AND (@PI_ClaimCreatedDateFrom IS NULL OR CONVERT(DATE,CreatedDate,103) >= CONVERT(DATE,@PI_ClaimCreatedDateFrom,103))
        AND (@PI_ClaimCreatedDateTo IS NULL OR CONVERT(DATE,CreatedDate,103) <= CONVERT(DATE,@PI_ClaimCreatedDateTo,103))
        AND (@PI_ClaimStatus IS NULL OR @PI_ClaimStatus = 0 OR ClaimStatus = @PI_ClaimStatus)
        AND (@PI_ClaimUpdatedDateFrom IS NULL OR CONVERT(DATE,UpdatedDate,103) >= CONVERT(DATE,@PI_ClaimUpdatedDateFrom,103))
        AND (@PI_ClaimUpdatedDateTo IS NULL OR CONVERT(DATE,UpdatedDate,103) <= CONVERT(DATE,@PI_ClaimUpdatedDateFrom,103))
        AND (@PI_UpdatedUser IS NULL OR UpdatedUser LIKE '%'+@PI_UpdatedUser+'%')
        AND (@PI_OrderNumber IS NULL OR OrderNumber LIKE '%'+@PI_OrderNumber+'%')
        AND IsDeleted = 0;
    
        SELECT
            DAT.ClaimId,
            DAT.ClaimNo,
            DAT.ClaimStatus,
            DAT.OrderNumber,
            DAT.BuyerName,
            DAT.CreatedDate,
            DAT.UpdatedUser,
            DAT.UpdatedDate
        FROM
            (
                SELECT
                    ClaimId,
                    ClaimNo,
                    ClaimStatus,
                    OrderNumber,
                    BuyerName,
                    CreatedDate,
                    UpdatedUser,
                    UpdatedDate,
                    ROW_NUMBER() OVER(ORDER BY UpdatedDate DESC) AS RowNum
                FROM
                    Claim
                WHERE
                    (@PI_ClaimNo IS NULL OR ClaimNo LIKE '%'+@PI_ClaimNo+'%')
                AND (@PI_BuyerName IS NULL OR BuyerName LIKE N'%'+@PI_BuyerName+'%')
                AND (@PI_ClaimCreatedDateFrom IS NULL OR CONVERT(DATE,CreatedDate,103) >= CONVERT(DATE,@PI_ClaimCreatedDateFrom,103))
                AND (@PI_ClaimCreatedDateTo IS NULL OR CONVERT(DATE,CreatedDate,103) <= CONVERT(DATE,@PI_ClaimCreatedDateTo,103))
                AND (@PI_ClaimStatus IS NULL OR @PI_ClaimStatus = 0 OR ClaimStatus = @PI_ClaimStatus)
                AND (@PI_ClaimUpdatedDateFrom IS NULL OR CONVERT(DATE,UpdatedDate,103) >= CONVERT(DATE,@PI_ClaimUpdatedDateFrom,103))
                AND (@PI_ClaimUpdatedDateTo IS NULL OR CONVERT(DATE,UpdatedDate,103) <= CONVERT(DATE,@PI_ClaimUpdatedDateFrom,103))
                AND (@PI_UpdatedUser IS NULL OR UpdatedUser LIKE '%'+@PI_UpdatedUser+'%')
                AND (@PI_OrderNumber IS NULL OR OrderNumber LIKE '%'+@PI_OrderNumber+'%')
                AND IsDeleted = 0
            ) AS DAT
        WHERE
            DAT.RowNum BETWEEN ((@PI_PageNo - 1) * @PI_PageSize + 1) AND (@PI_PageNo * @PI_PageSize)
    END TRY
    
    BEGIN CATCH
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_SearchClaim!';
    END CATCH
END
GO