USE [ApiBmc]
GO
IF OBJECT_ID('dbo.f_EncryptPassword', 'P') IS NOT NULL
    DROP PROC dbo.f_EncryptPassword
GO

CREATE FUNCTION dbo.f_EncryptPassword
(
    @PI_Password      VARCHAR(20)
)
RETURNS VARBINARY(100)
AS
BEGIN
    DECLARE @KeyMaster AS VARCHAR(10);
    
    SET @KeyMaster = 'Sendo';
    
    RETURN EncryptByPassPhrase(@KeyMaster, @PI_Password);
END
GO