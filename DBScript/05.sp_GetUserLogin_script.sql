USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_GetUserLogin', 'P') IS NOT NULL
    DROP PROC dbo.sp_GetUserLogin
GO

CREATE PROCEDURE dbo.sp_GetUserLogin
(
    @PI_UserName      VARCHAR(50),
    @PI_Password      VARCHAR(20),
    @PO_ErrorMessage  NVARCHAR(100) OUTPUT  
)
AS
BEGIN
    SET @PO_ErrorMessage = '';
    
    DECLARE @Total AS INT;

    BEGIN TRY
        SELECT
            @Total = COUNT(*)
        FROM
            Users
        WHERE
            UserName = @PI_UserName
        AND dbo.f_DecryptPassword(Password) = @PI_Password
        AND IsDeleted = 0
        
        IF @Total > 0
            BEGIN
                SELECT
                    UserId,
                    UserName,
                    Email,
                    FullName
                FROM
                    Users
                WHERE
                    UserName = @PI_UserName
                AND dbo.f_DecryptPassword(Password) = @PI_Password
                AND IsDeleted = 0
            END
        ELSE
            BEGIN
                SET @PO_ErrorMessage = N'User chưa được đăng ký.';
            END
    END TRY
    
    BEGIN CATCH
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_GetUserLogin!';
    END CATCH
END
GO