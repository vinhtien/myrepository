USE [ApiBmc]
GO

/****** Object:  Table [dbo].[ClaimEvidence] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ClaimEvidence](
    [ClaimEvidenceId] [int] IDENTITY(1,1) NOT NULL,
    [ClaimId] [int] NOT NULL,
    [DocumentUrl] [nvarchar](255) NOT NULL,
    [DocumentName] [nvarchar](255) NOT NULL,
    [DisplayOrder] [int] NULL,
    
    [IsDeleted] [bit] NOT NULL,
    [CreatedUser] [varchar](50) NULL,
    [CreatedDate] [datetime] NULL,
    [UpdatedUser] [varchar](50) NULL,
    [UpdatedDate] [datetime] NULL,

    CONSTRAINT [PK_ClaimEvidence] PRIMARY KEY CLUSTERED ([ClaimEvidenceId] ASC)
    WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClaimEvidence] ADD  CONSTRAINT [DF_ClaimEvidence_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO

SET ANSI_PADDING OFF
GO
