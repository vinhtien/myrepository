USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_GetNextClaim', 'P') IS NOT NULL
    DROP PROC dbo.sp_GetNextClaim
GO

CREATE PROCEDURE dbo.sp_GetNextClaim
(
    @PO_ErrorMessage  NVARCHAR(100) OUTPUT  
)
AS
BEGIN
    SET @PO_ErrorMessage = '';
    DECLARE @NextClaimId AS INT = CAST(IDENT_CURRENT('Claim') AS INT) + 1;
    
    IF @NextClaimId = 2
    BEGIN
        IF NOT EXISTS (SELECT ClaimId FROM Claim)
            BEGIN
                SET @NextClaimId = 1;
            END
    END
    
    BEGIN TRY
        SELECT
            @NextClaimId                        AS ClaimId, 
            dbo.fnc_CreateClaimNo(@NextClaimId) AS ClaimNo,
            1                                   AS ClaimStatus,
            1                                   AS ClaimType,
            1                                   AS Reason
    END TRY
    
    BEGIN CATCH
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_GetNextClaim!';
    END CATCH
END
GO