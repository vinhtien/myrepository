USE [ApiBmc]
GO

/****** Object:  Table [dbo].[Claim] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Claim](
    [ClaimId] [int] IDENTITY(1,1) NOT NULL,
    [ClaimNo] [varchar](50) NOT NULL,
    [ClaimStatus] [int] NOT NULL,
    [ClaimStatusDate] [datetime] NOT NULL,
    [ClaimType] [int] NOT NULL,
    [Reason] [int] NOT NULL,
    [ReasonDetail] [nvarchar](500) NULL,
    [OrderId] [int] NOT NULL,
    [OrderNumber] [varchar](50) NULL,
    [OrderDate] [datetime] NULL,
    [TotalPayment] [decimal](18, 4) NULL,
    [BuyerId] [int] NULL,
    [BuyerName] [nvarchar](100) NULL,
    [BuyerPhone] [varchar](20) NULL,
    [BuyerEmail] [varchar](255) NULL,
    [StoreId] [int] NULL,
    [StoreName] [nvarchar](400) NULL,
    [StorePhone] [varchar](20) NULL,
    [StoreEmail] [varchar](255) NULL,
    [CarrierName] [nvarchar](100) NULL,
    [DiscountAmount] [Decimal](18, 4) NULL,
    [ShippingFee] [Decimal](18, 4) NULL,
    [SendoSupportFee] [Decimal](18, 4) NULL,
    [SellerShippingFee] [Decimal](18, 4) NULL,
    
    [BuyerNote] [nvarchar](500) NULL,
    [SellerNote] [nvarchar](500) NULL,
    [CSNote] [nvarchar](500) NULL,
    [IsDeleted] [bit] NOT NULL,
    [CreatedUser] [varchar](50) NULL,
    [CreatedDate] [datetime] NULL,
    [UpdatedUser] [varchar](50) NULL,
    [UpdatedDate] [datetime] NULL,

    CONSTRAINT [PK_Claim] PRIMARY KEY CLUSTERED ([ClaimId] ASC)
    WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Claim] ADD  CONSTRAINT [DF_Claim_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Claim] ADD  CONSTRAINT [DF_Claim_ClaimStatus]  DEFAULT ((1)) FOR [ClaimStatus]
GO
ALTER TABLE [dbo].[Claim] ADD  CONSTRAINT [DF_Claim_ClaimStatusDate]  DEFAULT ((GETDATE())) FOR [ClaimStatusDate]
GO
ALTER TABLE [dbo].[Claim] ADD  CONSTRAINT [DF_Claim_ClaimType]  DEFAULT ((0)) FOR [ClaimType]
GO

SET ANSI_PADDING OFF
GO
