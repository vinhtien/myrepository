USE [ApiBmc]
GO

/****** Object:  Table [dbo].[ClaimJudgement] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ClaimJudgement](
    [JudgementId] [int] IDENTITY(1,1) NOT NULL,
    [ClaimId] [int] NOT NULL,
    [JudgementStatus] [int] NOT NULL,
    [JudgementStatusDate] [datetime] NOT NULL,
    [JudgementType] [int] NULL,
    [JudgementContent] [nvarchar](1000) NULL,
    [BuyerRefundAmount] [decimal](18, 4) NULL,
    [SellerRefundAmount] [decimal](18, 4) NULL,
    [IsSellerViolation] [bit] NOT NULL,
    [BuyerAgreement] [bit] NULL,
    [BuyerAgreementDate] [datetime] NULL,
    [SellerAgreement] [bit] NULL,
    [SellerAgreementDate] [datetime] NULL,
    [BuyerNote] [nvarchar](500) NULL,
    [SellerNote] [nvarchar](500) NULL,
    [CSNote] [nvarchar](500) NULL,
    [IsDeleted] [bit] NOT NULL,
    [CreatedUser] [varchar](50) NULL,
    [CreatedDate] [datetime] NULL,
    [UpdatedUser] [varchar](50) NULL,
    [UpdatedDate] [datetime] NULL,
    [IsCloseOrder] [bit] NULL,

    CONSTRAINT [PK_ClaimJudgement] PRIMARY KEY CLUSTERED ([JudgementId] ASC)
    WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClaimJudgement] ADD  CONSTRAINT [DF_ClaimJudgement_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ClaimJudgement] ADD  CONSTRAINT [DF_ClaimJudgement_JudgementStatus]  DEFAULT ((1)) FOR [JudgementStatus]
GO
ALTER TABLE [dbo].[ClaimJudgement] ADD  CONSTRAINT [DF_ClaimJudgement_JudgementStatusDate]  DEFAULT ((GETDATE())) FOR [JudgementStatusDate]
GO
ALTER TABLE [dbo].[ClaimJudgement] ADD  CONSTRAINT [DF_ClaimJudgement_IsSellerViolation]  DEFAULT ((0)) FOR [IsSellerViolation]
GO

SET ANSI_PADDING OFF
GO
