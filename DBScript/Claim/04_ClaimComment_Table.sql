USE [ApiBmc]
GO

/****** Object:  Table [dbo].[ClaimComment] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ClaimComment](
    [CommentId] [int] IDENTITY(1,1) NOT NULL,
    [ClaimId] [int] NOT NULL,
    [Comment] [nvarchar](255) NOT NULL,
    [IsDeleted] [bit] NOT NULL,
    [CreatedUser] [varchar](50) NULL,
    [CreatedDate] [datetime] NULL,
    [UpdatedUser] [varchar](50) NULL,
    [UpdatedDate] [datetime] NULL,

    CONSTRAINT [PK_ClaimComment] PRIMARY KEY CLUSTERED ([CommentId] ASC)
    WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClaimComment] ADD  CONSTRAINT [DF_ClaimComment_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO

SET ANSI_PADDING OFF
GO
