USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_GetClaimById', 'P') IS NOT NULL
    DROP PROC dbo.sp_GetClaimById
GO

CREATE PROCEDURE dbo.sp_GetClaimById
(
    @PI_ClaimId       INT,
    @PO_ErrorMessage  NVARCHAR(100) OUTPUT  
)
AS
BEGIN
    SET @PO_ErrorMessage = '';

    BEGIN TRY
        SELECT
            C.ClaimId,
            C.ClaimNo,
            C.ClaimStatus,
            C.ClaimStatusDate,
            C.ClaimType,
            C.Reason,
            C.ReasonDetail,
            C.OrderId,
            C.OrderNumber,
            C.OrderDate,
            C.TotalPayment,
            C.BuyerId,
            C.BuyerName,
            C.BuyerPhone,
            C.BuyerEmail,
            C.StoreId,
            C.StoreName,
            C.StorePhone,
            C.StoreEmail,
            C.CarrierName,
            C.DiscountAmount,
            C.ShippingFee,
            C.SendoSupportFee,
            C.SellerShippingFee,
            C.BuyerNote,
            C.SellerNote,
            C.CSNote,
            C.CreatedDate,
            C.UpdatedUser,
            C.UpdatedDate,
            --Thông tin bằng chứng của khiếu nại
            CE.ClaimEvidenceId,
            CE.DocumentUrl,
            CE.DocumentName,
            CE.DisplayOrder,
            --Thông tin phán quyết của khiếu nại
            CJ.JudgementId,
            CJ.JudgementStatus,
            CJ.JudgementType,
            CJ.JudgementContent,
            CJ.BuyerRefundAmount,
            CJ.SellerRefundAmount,
            CJ.IsSellerViolation
        FROM
            Claim C
            LEFT JOIN ClaimEvidence CE ON C.ClaimId = CE.ClaimId AND CE.IsDeleted = 0
            LEFT JOIN ClaimJudgement CJ ON C.ClaimId = CJ.ClaimId AND CJ.IsDeleted = 0
        WHERE
            C.ClaimId = @PI_ClaimId
        AND C.IsDeleted = 0

    END TRY
    
    BEGIN CATCH
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_GetClaimById!';
    END CATCH
END
GO