USE [ApiBmc]
GO
IF OBJECT_ID('dbo.sp_GetClaimComment', 'P') IS NOT NULL
    DROP PROC dbo.sp_GetClaimComment
GO

CREATE PROCEDURE dbo.sp_GetClaimComment
(
    @PI_ClaimId       INT,
    @PO_ErrorMessage  NVARCHAR(100) OUTPUT  
)
AS
BEGIN
    SET @PO_ErrorMessage = '';

    BEGIN TRY
        SELECT
            ClaimId,
            Comment,
            CreatedUser,
            CreatedDate
        FROM
            ClaimComment
        WHERE
            ClaimId = @PI_ClaimId
        AND IsDeleted = 0
        ORDER BY
            CreatedDate
    END TRY
    
    BEGIN CATCH
        SET @PO_ErrorMessage = N'Xảy ra lỗi tại sp_GetClaimComment!';
    END CATCH
END
GO