﻿using System.ComponentModel.DataAnnotations;

namespace InsideBMC.API
{
    public enum ClaimReason : int
    {
        [Display(Name = "Sản phẩm không giống mô tả")]
        Reason_1 = 1,
        [Display(Name = "Vận chuyển cập nhật sai")]
        Reason_2 = 2,
        [Display(Name = "Sai màu, sai kích thước")]
        Reason_3 = 3,
        [Display(Name = "Hàng bị lỗi, bể vỡ, móp méo")]
        Reason_4 = 4,
        [Display(Name = "Giao nhầm sản phẩm")]
        Reason_5 = 5,
        [Display(Name = "Khác")]
        Reason_6 = 6
    }
}