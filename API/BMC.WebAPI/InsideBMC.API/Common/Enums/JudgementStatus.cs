﻿using System.ComponentModel.DataAnnotations;

namespace InsideBMC.API
{
    public enum JudgementStatus : int
    {
        [Display(Name = "Chưa tạo phán quyết")]
        NOT_YET = 0,

        [Display(Name = "Đã tạo phán quyết")]
        CREATE = 1,

        [Display(Name = "Đang trong quá trình đổi trả bổ sung")]
        PROCESS = 2,

        [Display(Name = "Đã áp dụng phán quyết")]
        APPLY = 3
    }
}