﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InsideBMC.API
{
    enum ClaimStatus
    {
        NEW = 1,                      // 1:  Khiếu nại mới
        CS_REQUEST_UPDATE = 2,        // 2:  CS yêu cầu buyer bổ sung bằng chứng
        BUYER_UPDATED = 3,            // 3:  Buyer đã bổ sung bằng chứng
        CS_REQUEST_NEGOTIATE = 4,     // 4:  CS yêu cầu seller thỏa thuận với buyer
        SELLER_DENY_NEGOTIATE = 5,    // 5:  Seller không chấp nhận thỏa thuận
        SELLER_ACCEPT_NEGOTIATE = 6,  // 6:  Seller chấp nhận thỏa thuận
        BUYER_DENY_NEGOTIATE = 7,     // 7:  Buyer không chấp nhận thỏa thuận
        BUYER_ACCEPT_NEGOTIATE = 8,   // 8:  Buyer chấp nhận thỏa thuận
        CS_MAKE_DICISION = 9,         // 9:  CS đưa ra phán quyết
        WAIT_CLOSE = 10,              // 10: Khiếu nại chờ CS duyệt để đóng
        CLOSED = 11,                  // 11: Khiếu nại đã được đóng
        CANCEL = 12                   // 12: Khiếu nại đã bị hủy
    }
}