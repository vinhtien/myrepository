﻿using InsideBMC.API.Models.Complaint;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace InsideBMC.API
{
    public static class Utils
    {
        /// <summary>
        /// Convert list evidence thành datatable
        /// </summary>
        /// <param name="claimEvidences"></param>
        /// <returns></returns>
        public static DataTable ConvertClaimEvidenceToDataTable(List<ClaimDetailModel> claimEvidences)
        {
            DataTable dt = null;

            if (null != claimEvidences && claimEvidences.Count > 0)
            {
                dt = new DataTable();
                dt.Columns.Add("ClaimId", typeof(int));
                dt.Columns.Add("DocumentUrl", typeof(string));
                dt.Columns.Add("DocumentName", typeof(string));
                dt.Columns.Add("DisplayOrder", typeof(int));

                foreach (ClaimDetailModel claimEvidence in claimEvidences)
                {
                    DataRow dr = dt.NewRow();
                    dr["ClaimId"] = claimEvidence.ClaimId;
                    dr["DocumentUrl"] = claimEvidence.DocumentUrl;
                    dr["DocumentName"] = claimEvidence.DocumentName;
                    dr["DisplayOrder"] = claimEvidence.DisplayOrder;
                    dt.Rows.Add(dr);
                }
            }

            return dt;
        }

        public static string OpcPostJson(string requestAddress, object param)
        {
            //Setup request
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(requestAddress));
            httpWebRequest.Accept = "application/json";
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            string apiKey = "1610335375";
            string secretKey = "cdd16d4a93fc4b02a0730e1064d0bd62";
            //gen key
            using (HMACSHA256 hmac = new HMACSHA256(Convert.FromBase64String(secretKey)))
            {
                var content = JsonConvert.SerializeObject(param);
                var byteContent = Encoding.UTF8.GetBytes(string.Format("{0}{1}{2}", apiKey, secretKey, content));
                var signatureBytes = hmac.ComputeHash(byteContent);
                httpWebRequest.Headers.Add("Authorization", string.Format("{0}:{1}", apiKey, Convert.ToBase64String(signatureBytes)));
            }

            httpWebRequest.Method = WebRequestMethods.Http.Post;

            string result = string.Empty;
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                if (param != null)
                {
                    streamWriter.Write(JsonConvert.SerializeObject(param));
                }
                streamWriter.Flush();
                streamWriter.Close();

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            return result;
        }

        public static string OpcGetJson(this string requestAddress)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(requestAddress));
            string apiKey = "1610335375";
            string secretKey = "cdd16d4a93fc4b02a0730e1064d0bd62";
            using (HMACSHA256 hmac = new HMACSHA256(Convert.FromBase64String(secretKey)))
            {
                var byteContent = Encoding.UTF8.GetBytes(string.Format("{0}{1}{2}", apiKey, secretKey, ""));
                var signatureBytes = hmac.ComputeHash(byteContent);
                httpWebRequest.Headers.Add("Authorization", string.Format("{0}:{1}", apiKey, Convert.ToBase64String(signatureBytes)));
            }
            var httpResponse = httpWebRequest.GetResponse();
            string result = string.Empty;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }

        public static string GetAttributeNameOfEnum(Type enumType, int? value)
        {
            if (value == null || Enum.GetName(enumType, value) == null)
            {
                return string.Empty;
            }

            var enumName = enumType.GetField(Enum.GetName(enumType, value));
            DisplayAttribute displayAtt = enumName.GetCustomAttributes(typeof(DisplayAttribute), false).FirstOrDefault() as DisplayAttribute;

            if (null != displayAtt)
            {
                return displayAtt.GetName();
            }
            return string.Empty;
        }
    }
}