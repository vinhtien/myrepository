﻿using Owin;

namespace InsideBMC.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureStartupAuth(app);
        }
    }
}