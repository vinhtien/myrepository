﻿using Dapper;
using InsideBMC.API.Models;
using InsideBMC.API.Models.Complaint;
using InsideBMC.API.Models.Email;
using InsideBMC.API.Models.Order;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace InsideBMC.API.Services
{
    public class ComplaintService
    {
        public OrderService service = new OrderService();

        public const string TO_BUYER_CLAIM_INFO_NEW = "To.Buyer.Claim.Info.New";
        public const string TO_BUYER_CLAIM_REQUEST_UPDATE = "To.Buyer.Claim.Request.Update";
        public const string TO_BUYER_CLAIM_INFO_ACCEPT = "To.Buyer.Claim.Info.Accept";
        public const string TO_SELLER_CLAIM_INFO_ACCEPT = "To.Seller.Claim.Info.Accept";
        public const string TO_BUYER_CLAIM_INFO_CANCEL = "To.Buyer.Claim.Info.Cancel";

        #region Inside

        /// <summary>
        /// Get chi tiết khiếu nại theo Id
        /// </summary>
        /// <param name="claimId"></param>
        /// <returns></returns>
        public ResultData GetClaimById(int claimId)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_ClaimId", claimId, DbType.Int32);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure get danh sách khiếu nại theo salesOrderId
            string errorMessage = string.Empty;
            List<ClaimDetailModel> claimEvidences = DBConnection.QueryByStoreProcedure<ClaimDetailModel>("sp_GetClaimById", out errorMessage, param);

            if (null != claimEvidences && string.IsNullOrEmpty(errorMessage) && claimEvidences.Count > 0)
            {
                ClaimModel claimModel = new ClaimModel();

                //Set thông tin khiếu nại
                claimModel.ClaimId = claimEvidences[0].ClaimId;
                claimModel.ClaimNo = claimEvidences[0].ClaimNo;
                claimModel.ClaimStatus = claimEvidences[0].ClaimStatus;
                claimModel.ClaimStatusDate = claimEvidences[0].ClaimStatusDate;
                claimModel.ClaimType = claimEvidences[0].ClaimType;
                claimModel.Reason = claimEvidences[0].Reason;
                claimModel.ReasonDetail = claimEvidences[0].ReasonDetail;

                claimModel.OrderId = claimEvidences[0].OrderId;
                claimModel.OrderNumber = claimEvidences[0].OrderNumber;
                claimModel.OrderDate = claimEvidences[0].OrderDate;
                claimModel.TotalPayment = claimEvidences[0].TotalPayment;
                claimModel.BuyerId = claimEvidences[0].BuyerId;
                claimModel.BuyerName = claimEvidences[0].BuyerName;
                claimModel.BuyerPhone = claimEvidences[0].BuyerPhone;
                claimModel.BuyerEmail = claimEvidences[0].BuyerEmail;
                claimModel.StoreId = claimEvidences[0].StoreId;
                claimModel.StoreName = claimEvidences[0].StoreName;
                claimModel.StorePhone = claimEvidences[0].StorePhone;
                claimModel.StoreEmail = claimEvidences[0].StoreEmail;
                claimModel.CarrierName = claimEvidences[0].CarrierName;
                claimModel.DiscountAmount = claimEvidences[0].DiscountAmount;
                claimModel.ShippingFee = claimEvidences[0].ShippingFee;
                claimModel.SendoSupportFee = claimEvidences[0].SendoSupportFee;
                claimModel.SellerShippingFee = claimEvidences[0].SellerShippingFee;

                claimModel.BuyerNote = claimEvidences[0].BuyerNote;
                claimModel.CSNote = claimEvidences[0].CSNote;
                claimModel.SellerNote = claimEvidences[0].SellerNote;
                claimModel.CreatedDate = claimEvidences[0].CreatedDate;
                claimModel.UpdatedUser = claimEvidences[0].UpdatedUser;
                claimModel.UpdatedDate = claimEvidences[0].UpdatedDate;

                //Set thông tin phán quyết của khiếu nại
                claimModel.JudgementId = claimEvidences[0].JudgementId;
                claimModel.JudgementStatus = claimEvidences[0].JudgementStatus;
                claimModel.JudgementType = claimEvidences[0].JudgementType;
                claimModel.JudgementContent = claimEvidences[0].JudgementContent;
                claimModel.BuyerRefundAmount = claimEvidences[0].BuyerRefundAmount;
                claimModel.SellerRefundAmount = claimEvidences[0].SellerRefundAmount;
                claimModel.IsSellerViolation = claimEvidences[0].IsSellerViolation;

                //Set thông tin bằng chứng khiếu nại
                if (claimEvidences.Count == 1 && claimEvidences[0].ClaimEvidenceId == 0)
                {
                    claimModel.ClaimEvidences = null;
                }
                else
                {
                    claimModel.ClaimEvidences = claimEvidences;
                }

                return new ResultData(isError: false, message: string.Empty, data: claimModel);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        /// <summary>
        /// Lấy ClaimId và ClaimNo trước khi tạo khiếu nại
        /// </summary>
        /// <returns></returns>
        public ResultData GetNextClaim()
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure get danh sách khiếu nại theo salesOrderId
            string errorMessage = string.Empty;
            ClaimModel nextClaimModel = DBConnection.QueryByStoreProcedure<ClaimModel>("sp_GetNextClaim", out errorMessage, param).FirstOrDefault();

            if (null != nextClaimModel)
            {
                return new ResultData(isError: false, message: string.Empty, data: nextClaimModel);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        /// <summary>
        /// Search danh sách khiếu nại theo điều kiện
        /// </summary>
        /// <param name="claimSearchForm">điều kiện search</param>
        /// <returns></returns>
        public ResultData SearchClaim(ClaimSearchForm claimSearchForm)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_ClaimNo", string.IsNullOrEmpty(claimSearchForm.ClaimNo) ? null : claimSearchForm.ClaimNo, size: 50);
            param.Add("@PI_BuyerName", string.IsNullOrEmpty(claimSearchForm.BuyerName) ? null : claimSearchForm.BuyerName, size: 100);
            param.Add("@PI_ClaimCreatedDateFrom", claimSearchForm.ClaimCreatedDateFrom, DbType.DateTime);
            param.Add("@PI_ClaimCreatedDateTo", claimSearchForm.ClaimCreatedDateTo, DbType.DateTime);
            param.Add("@PI_ClaimStatus", claimSearchForm.ClaimStatus, DbType.Int32);
            param.Add("@PI_ClaimUpdatedDateFrom", claimSearchForm.ClaimUpdatedDateFrom, DbType.DateTime);
            param.Add("@PI_ClaimUpdatedDateTo", claimSearchForm.ClaimUpdatedDateTo, DbType.DateTime);
            param.Add("@PI_UpdatedUser", string.IsNullOrEmpty(claimSearchForm.UpdatedUser) ? null : claimSearchForm.UpdatedUser, size: 50);
            param.Add("@PI_OrderNumber", string.IsNullOrEmpty(claimSearchForm.OrderNumber) ? null : claimSearchForm.OrderNumber, size: 50);
            param.Add("@PI_PageNo", claimSearchForm.PageNo, DbType.Int32);
            param.Add("@PI_PageSize", claimSearchForm.PageSize, DbType.Int32);

            param.Add("@PO_TotalCount", dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure get danh sách khiếu nại theo salesOrderId
            int totalCount = 0;
            string errorMessage = string.Empty;
            List<ClaimSearchModel> listClaim = DBConnection.QueryByStoreProcedurePaging<ClaimSearchModel>("sp_SearchClaim", out errorMessage, out totalCount, param);

            if (null != listClaim && string.IsNullOrEmpty(errorMessage))
            {
                return new ResultData(isError: false, message: string.Empty, totalCount: totalCount, data: listClaim);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, totalCount: totalCount, data: null);
            }
        }

        /// <summary>
        /// API Tạo khiếu nại
        /// </summary>
        /// <param name="claimModel"></param>
        /// <param name="username">username của user login</param>
        /// <returns></returns>
        public ResultData CreateClaim(ClaimModel claimModel, string username)
        {
            //Tạm thời comment gọi đóng băng đơn hàng do bên OPC chưa chạy được
            //--------------------------------------------------------------------------------------

            //Gọi OPC đóng băng đơn hàng
            //ResultData freezeOrderResult = service.FreezeOrder(claimModel.OrderNumber, username);

            //Trường hợp đóng băng đơn hàng thành công mới thực hiện tạo khiếu nại
            //if (freezeOrderResult.IsError == false && (bool)freezeOrderResult.Data == true)
            //{

            //--------------------------------------------------------------------------------------

            //Convert list claim evidence thành datatable sử dụng truyền parameter của store procedure
            DataTable claimEvidenceDt = Utils.ConvertClaimEvidenceToDataTable(claimModel.ClaimEvidences);

            //Tạo parameter
            var param = new DynamicParameters();
            param.Add("@PI_ClaimType", claimModel.ClaimType, DbType.Int32);
            param.Add("@PI_Reason", claimModel.Reason, DbType.Int32);
            param.Add("@PI_ReasonDetail", claimModel.ReasonDetail, size: 500);
            if (null != claimEvidenceDt && claimEvidenceDt.Rows.Count > 0)
            {
                param.Add("@PI_ClaimEvidences", claimEvidenceDt.AsTableValuedParameter("ClaimEvidenceType"));
            }
            param.Add("@PI_OrderId", claimModel.OrderId, DbType.Int32);
            param.Add("@PI_OrderNumber", claimModel.OrderNumber);
            param.Add("@PI_OrderDate", claimModel.OrderDate, DbType.DateTime);
            param.Add("@PI_TotalPayment", claimModel.TotalPayment, DbType.Decimal);
            param.Add("@PI_BuyerId", claimModel.BuyerId, DbType.Int32);
            param.Add("@PI_BuyerName", claimModel.BuyerName);
            param.Add("@PI_BuyerPhone", claimModel.BuyerPhone);
            param.Add("@PI_BuyerEmail", claimModel.BuyerEmail);
            param.Add("@PI_StoreId", claimModel.StoreId, DbType.Int32);
            param.Add("@PI_StoreName", claimModel.StoreName);
            param.Add("@PI_StorePhone", claimModel.StorePhone);
            param.Add("@PI_StoreEmail", claimModel.StoreEmail);
            param.Add("@PI_CarrierName", claimModel.CarrierName);
            param.Add("@PI_DiscountAmount", claimModel.DiscountAmount, DbType.Decimal);
            param.Add("@PI_ShippingFee", claimModel.ShippingFee, DbType.Decimal);
            param.Add("@PI_SendoSupportFee", claimModel.SendoSupportFee, DbType.Decimal);
            param.Add("@PI_SellerShippingFee", claimModel.SellerShippingFee, DbType.Decimal);

            param.Add("@PI_User", username, size: 50);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);
            param.Add("@PO_Id", dbType: DbType.Int32, direction: ParameterDirection.Output);

            //Thực thi store procedure (insert/update/delete)
            string errorMessage = string.Empty;
            int claimId = 0;
            int rowCount = DBConnection.ExecuteStoreProcedureReturnId("sp_CreateClaim", out errorMessage, out claimId, param);

            if (rowCount > 0 && claimId > 0)
            {
                //Get thông tin chi tiết khiếu nại
                ResultData claimInfoResult = GetClaimById(claimId);
                if (claimInfoResult.IsError == false)
                {
                    ClaimModel claimInfo = (ClaimModel)claimInfoResult.Data;

                    //Send mail thông báo cho buyer có khiếu nại mới
                    SendEmailByTemplate(claimInfo, TO_BUYER_CLAIM_INFO_NEW);

                    return new ResultData(isError: true, message: "Tạo khiếu nại thành công!", totalCount: 1, data: claimInfoResult.Data);
                }
                else
                {
                    return new ResultData(isError: true, message: claimInfoResult.Message, data: null);
                }
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }

            //Tạm thời comment gọi đóng băng đơn hàng do bên OPC chưa chạy được
            //--------------------------------------------------------------------------------------

            //}
            //Trường hợp đóng băng đơn hàng không thành công
            //else
            //{
            //    return new ResultData(isError: true, message: freezeOrderResult.Message, totalCount: 0, data: null);
            //}

            //--------------------------------------------------------------------------------------
        }

        /// <summary>
        /// Cập nhật thông tin khiếu nại (Lý do và chi tiết khiếu nại)
        /// </summary>
        /// <param name="claimModel"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public ResultData UpdateClaim(ClaimModel claimModel, string username)
        {
            //Convert list claim evidence thành datatable sử dụng truyền parameter của store procedure
            DataTable claimEvidenceDt = Utils.ConvertClaimEvidenceToDataTable(claimModel.ClaimEvidences);

            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_ClaimId", claimModel.ClaimId, DbType.Int32);
            param.Add("@PI_ClaimType", claimModel.ClaimType, DbType.Int32);
            param.Add("@PI_Reason", claimModel.Reason, DbType.Int32);
            param.Add("@PI_ReasonDetail", claimModel.ReasonDetail, size: 500);
            if (null != claimEvidenceDt && claimEvidenceDt.Rows.Count > 0)
            {
                param.Add("@PI_ClaimEvidences", claimEvidenceDt.AsTableValuedParameter("ClaimEvidenceType"));
            }
            param.Add("@PI_User", username, size: 50);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure (insert/update/delete)
            string errorMessage = string.Empty;
            int rowCount = DBConnection.ExecuteCommandByStoreProcedure("sp_UpdateClaim", out errorMessage, param);

            if (rowCount > 0)
            {
                return new ResultData(isError: false, message: "Cập nhật khiếu nại thành công!", data: null);
            }
            return new ResultData(isError: true, message: errorMessage, data: null);
        }

        /// <summary>
        /// Bổ sung bằng chứng của khiếu nại
        /// </summary>
        /// <param name="claimModel"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public ResultData UpdateClaimEvidences(ClaimModel claimModel, string username)
        {
            //Convert list claim evidence thành datatable sử dụng truyền parameter của store procedure
            DataTable claimEvidenceDt = Utils.ConvertClaimEvidenceToDataTable(claimModel.ClaimEvidences);

            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_ClaimId", claimModel.ClaimId, DbType.Int32);
            if (null != claimEvidenceDt && claimEvidenceDt.Rows.Count > 0)
            {
                param.Add("@PI_ClaimEvidences", claimEvidenceDt.AsTableValuedParameter("ClaimEvidenceType"));
            }
            param.Add("@PI_User", username, size: 50);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure (insert/update/delete)
            string errorMessage = string.Empty;
            int rowCount = DBConnection.ExecuteCommandByStoreProcedure("sp_UpdateClaimEvidences", out errorMessage, param);

            if (rowCount > 0)
            {
                var returnModel = new
                {
                    ClaimId = claimModel.ClaimId,
                    ClaimEvidences = claimModel.ClaimEvidences.Select(x => new { x.DocumentUrl, x.DocumentName }).ToList()
                };
                return new ResultData(isError:false, message: "Bổ sung bằng chứng của khiếu nại thành công!", data: returnModel);
            }
            return new ResultData(isError: true, message: errorMessage, data: null);
        }

        /// <summary>
        /// Cập nhật trạng thái của khiếu nại
        /// </summary>
        /// <param name="claimModel"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public ResultData UpdateClaimStatus(ClaimModel claimModel, string username)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_ClaimId", claimModel.ClaimId, DbType.Int32);
            param.Add("@PI_ClaimStatus", claimModel.ClaimStatus, DbType.Int32);
            param.Add("@PI_User", username, size: 50);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure (insert/update/delete)
            string errorMessage = string.Empty;
            int rowCount = DBConnection.ExecuteCommandByStoreProcedure("sp_UpdateClaimStatus", out errorMessage, param);

            if (rowCount > 0)
            {
                //Gửi email template theo từng trạng thái của khiếu nại
                SendEmailByClaimStatus(claimModel.ClaimId, claimModel.ClaimStatus);

                var returnModel = new
                {
                    ClaimId = claimModel.ClaimId,
                    ClaimStatus = claimModel.ClaimStatus
                };
                return new ResultData(isError: false, message: "Cập nhật trạng thái khiếu nại thành công!", data: returnModel);
            }
            return new ResultData(isError: true, message: errorMessage, data: null);
        }

        /// <summary>
        /// API tạo thông tin phán quyết
        /// </summary>
        /// <param name="claimModel"></param>
        /// <param name="username">username của user login</param>
        /// <returns></returns>
        public ResultData InsertJudgement(ClaimModel claimModel, string username)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_ClaimId", claimModel.ClaimId, DbType.Int32);
            param.Add("@PI_JudgementStatus", claimModel.JudgementStatus, DbType.Int32);
            param.Add("@PI_JudgementType", claimModel.JudgementType, DbType.Int32);
            param.Add("@PI_JudgementContent", claimModel.JudgementContent, size: 1000);
            param.Add("@PI_BuyerRefundAmount", claimModel.BuyerRefundAmount, DbType.Decimal);
            param.Add("@PI_SellerRefundAmount", claimModel.SellerRefundAmount, DbType.Decimal);
            param.Add("@PI_IsSellerViolation", claimModel.IsSellerViolation, DbType.Boolean);
            param.Add("@PI_User", username, size: 50);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure (insert/update/delete)
            string errorMessage = string.Empty;
            int rowCount = DBConnection.ExecuteCommandByStoreProcedure("sp_InsertJudgement", out errorMessage, param);

            if (rowCount > 0)
            {
                return new ResultData(isError: false, message: "Tạo phán quyết thành công!", data: null);
            }
            return new ResultData(isError: true, message: errorMessage, data: null);
        }

        /// <summary>
        /// API cập nhật thông tin phán quyết
        /// </summary>
        /// <param name="claimModel"></param>
        /// <param name="username">username của user login</param>
        /// <returns></returns>
        public ResultData UpdateJudgement(ClaimModel claimModel, string username)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_ClaimId", claimModel.ClaimId, DbType.Int32);
            param.Add("@PI_JudgementType", claimModel.JudgementType, DbType.Int32);
            param.Add("@PI_JudgementStatus", claimModel.JudgementStatus, DbType.Int32);
            param.Add("@PI_JudgementContent", claimModel.JudgementContent, size: 1000);
            param.Add("@PI_BuyerRefundAmount", claimModel.BuyerRefundAmount, DbType.Decimal);
            param.Add("@PI_SellerRefundAmount", claimModel.SellerRefundAmount, DbType.Decimal);
            param.Add("@PI_IsSellerViolation", claimModel.IsSellerViolation, DbType.Boolean);
            param.Add("@PI_User", username, size: 50);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure (insert/update/delete)
            string errorMessage = string.Empty;
            int rowCount = DBConnection.ExecuteCommandByStoreProcedure("sp_UpdateJudgement", out errorMessage, param);

            if (rowCount > 0)
            {
                return new ResultData(isError: false, message: "Cập nhật thông tin phán quyết thành công!", data: null);
            }
            return new ResultData(isError: true, message: errorMessage, data: null);
        }

        /// <summary>
        /// Cập nhật trạng thái của phán quyết
        /// </summary>
        /// <param name="claimModel"></param>
        /// <param name="username">username của user login</param>
        /// <returns></returns>
        public ResultData UpdateJudgementStatus(ClaimModel claimModel, string username)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_ClaimId", claimModel.ClaimId, DbType.Int32);
            param.Add("@PI_JudgementStatus", claimModel.JudgementStatus, DbType.Int32);
            param.Add("@PI_User", username, size: 50);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure (insert/update/delete)
            string errorMessage = string.Empty;
            int rowCount = DBConnection.ExecuteCommandByStoreProcedure("sp_UpdateJudgementStatus", out errorMessage, param);

            if (rowCount > 0)
            {
                return new ResultData(isError: false, message: "Cập nhật trạng thái phán quyết thành công!", data: null);
            }
            return new ResultData(isError: true, message: errorMessage, data: null);
        }

        /// <summary>
        /// Search danh sách đơn hàng để khiếu nại
        /// </summary>
        /// <param name="orderSearchForm">điều kiện search</param>
        /// <returns></returns>
        public ResultData SearchOrderForComplaint(OrderSearchForm orderSearchForm)
        {
            try
            {
                //Tạo paramter
                var param = new
                {
                    OrderNumber = orderSearchForm.OrderNumber,
                    BuyerName = orderSearchForm.BuyerName,
                    OrderStatus = new List<int>() { 7 },
                    FromDate = orderSearchForm.FromDate,
                    ToDate = orderSearchForm.ToDate,
                    PageSize = orderSearchForm.PageSize,
                    PageNumber = orderSearchForm.PageNumber
                };

                //OPC API address
                string requestAddress = "http://api-opc.test.sendo.vn/api/SalesOrder/searchorder";

                //Gọi Api bằng phương thức Post
                string responseString = Utils.OpcPostJson(requestAddress, param);

                if (!string.IsNullOrEmpty(responseString))
                {
                    //Deserialize từ json data thành object
                    OpcResultData responseResult = JsonConvert.DeserializeObject<OpcResultData>(responseString);

                    //Trường hợp gọi API thành công
                    if (responseResult.Status == 200)
                    {
                        OpcResultData resultData = JsonConvert.DeserializeObject<OpcResultData>(responseResult.Data.ToString());

                        //Convert data thành object
                        JArray jsonArrayData = (JArray)resultData.Results;
                        List<OrderModel> listOrder = jsonArrayData.ToObject<List<OrderModel>>();

                        return new ResultData(isError: false, message: string.Empty, totalCount: resultData.TotalRecords, data: listOrder);
                    }
                    else
                    {
                        return new ResultData(isError: true, message: responseResult.Message, totalCount: 0, data: null);
                    }
                }
                else
                {
                    return new ResultData(isError: true, message: "Call api/searchorder error!", totalCount: 0, data: null);
                }
            }
            catch (Exception ex)
            {
                return new ResultData(isError: true, message: ex.Message, totalCount: 0, data: null);
            }
        }

        /// <summary>
        /// CS insert comment
        /// </summary>
        /// <param name="claimModel"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public ResultData InsertComment(ClaimModel claimModel, string username)
        {
            //Tạo parameter
            var param = new DynamicParameters();
            param.Add("@PI_ClaimId", claimModel.ClaimId, DbType.Int32);
            param.Add("@PI_Comment", claimModel.Comment, size: 255);

            param.Add("@PI_User", username, size: 50);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure (insert/update/delete)
            string errorMessage = string.Empty;
            int rowCount = DBConnection.ExecuteCommandByStoreProcedure("sp_InsertComment", out errorMessage, param);

            if (rowCount > 0)
            {
                ResultData listCommentResult = GetClaimComment(claimModel.ClaimId);
                if (listCommentResult.IsError == false)
                {
                    return new ResultData(isError: false, message: string.Empty, totalCount: listCommentResult.TotalCount, data: listCommentResult.Data);
                }
                else
                {
                    return new ResultData(isError: false, message: listCommentResult.Message, totalCount: 0, data: null);
                }
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        /// <summary>
        /// Get danh sách comment của khiếu nại
        /// </summary>
        /// <param name="claimId"></param>
        /// <returns></returns>
        public ResultData GetClaimComment(int claimId)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_ClaimId", claimId, DbType.Int32);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure get danh sách khiếu nại theo salesOrderId
            string errorMessage = string.Empty;
            List<CommentModel> listComment = DBConnection.QueryByStoreProcedure<CommentModel>("sp_GetClaimComment", out errorMessage, param);

            if (null != listComment && string.IsNullOrEmpty(errorMessage))
            {
                return new ResultData(isError: false, message: string.Empty, totalCount: listComment.Count, data: listComment);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        public ResultData GetClaimListByBuyerId(int buyerId, int pageNo, int pageSize)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_BuyerId", buyerId, DbType.Int32);
            param.Add("@PI_PageNo", pageNo, DbType.Int32);
            param.Add("@PI_PageSize", pageSize, DbType.Int32);

            param.Add("@PO_TotalCount", dbType: DbType.Int32, direction: ParameterDirection.Output);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure get danh sách khiếu nại theo salesOrderId
            string errorMessage = string.Empty;
            int totalCount = 0;
            List<ClaimModel> listClaim = DBConnection.QueryByStoreProcedurePaging<ClaimModel>("sp_GetClaimListByBuyerId", out errorMessage, out totalCount, param);

            if (null != listClaim && string.IsNullOrEmpty(errorMessage))
            {
                return new ResultData(isError: false, message: string.Empty, totalCount: totalCount, data: listClaim);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        #endregion

        #region Buyer

        /// <summary>
        /// 01 API lấy danh sách sản phẩm theo orderId
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public ResultData GetProductListByOrderId(int orderId)
        {
            try
            {
                //OPC API address
                string requestAddress = string.Format("http://api-opc.test.sendo.vn/api/SalesOrder/getorderdetail/{0}", orderId);

                //Gọi Api bằng phương thức Post
                string responseString = Utils.OpcGetJson(requestAddress);

                if (!string.IsNullOrEmpty(responseString))
                {
                    //Deserialize từ json data thành object
                    OpcResultData responseResult = JsonConvert.DeserializeObject<OpcResultData>(responseString);

                    //Trường hợp gọi API thành công
                    if (responseResult.Status == 200)
                    {
                        OpcResultData dataResult = JsonConvert.DeserializeObject<OpcResultData>(JsonConvert.SerializeObject(responseResult.Data));

                        //Convert data ListProduct thành Object
                        JArray jsonArrayData = (JArray)dataResult.ListProduct;
                        List<ProductModel> listProduct = jsonArrayData.ToObject<List<ProductModel>>();

                        return new ResultData(isError: false, message: string.Empty, totalCount: listProduct.Count, data: listProduct);
                    }
                    else
                    {
                        return new ResultData(isError: true, message: responseResult.Message, totalCount: 0, data: null);
                    }
                }
                else
                {
                    return new ResultData(isError: true, message: "Call api/getorderdetail/{id} error!", totalCount: 0, data: null);
                }
            }
            catch (Exception ex)
            {
                return new ResultData(isError: true, message: ex.Message, totalCount: 0, data: null);
            }
        }

        /// <summary>
        /// 02 API Tạo khiếu nại từ Buyer
        /// </summary>
        /// <param name="claimModel"></param>
        /// <param name="username">username của user login</param>
        /// <returns></returns>
        public ResultData CreateClaimFromBuyer(ClaimModel claimModel, string username)
        {
            //Gọi OPC đóng băng đơn hàng
            ResultData freezeOrderResult = service.FreezeOrder(claimModel.OrderNumber, username);

            //Trường hợp đóng băng đơn hàng thành công mới thực hiện tạo khiếu nại
            if (freezeOrderResult.IsError == false && (bool)freezeOrderResult.Data == true)
            {
                //Convert list claim evidence thành datatable sử dụng truyền parameter của store procedure
                DataTable claimEvidenceDt = Utils.ConvertClaimEvidenceToDataTable(claimModel.ClaimEvidences);

                //Tạo parameter
                var param = new DynamicParameters();
                param.Add("@PI_ClaimType", claimModel.ClaimType, DbType.Int32);
                param.Add("@PI_Reason", claimModel.Reason, DbType.Int32);
                param.Add("@PI_ReasonDetail", claimModel.ReasonDetail, size: 500);
                if (null != claimEvidenceDt && claimEvidenceDt.Rows.Count > 0)
                {
                    param.Add("@PI_ClaimEvidences", claimEvidenceDt.AsTableValuedParameter("ClaimEvidenceType"));
                }
                param.Add("@PI_OrderId", claimModel.OrderId, DbType.Int32);
                param.Add("@PI_OrderNumber", claimModel.OrderNumber);
                param.Add("@PI_OrderDate", claimModel.OrderDate, DbType.DateTime);
                param.Add("@PI_TotalPayment", claimModel.TotalPayment, DbType.Decimal);
                param.Add("@PI_BuyerId", claimModel.BuyerId, DbType.Int32);
                param.Add("@PI_BuyerName", claimModel.BuyerName);
                param.Add("@PI_BuyerPhone", claimModel.BuyerName);
                param.Add("@PI_BuyerEmail", claimModel.BuyerName);
                param.Add("@PI_StoreId", claimModel.StoreId, DbType.Int32);
                param.Add("@PI_StoreName", claimModel.StoreName);
                param.Add("@PI_StorePhone", claimModel.BuyerName);
                param.Add("@PI_StoreEmail", claimModel.BuyerName);
                param.Add("@PI_CarrierName", claimModel.CarrierName);
                param.Add("@PI_DiscountAmount", claimModel.DiscountAmount, DbType.Decimal);
                param.Add("@PI_ShippingFee", claimModel.ShippingFee, DbType.Decimal);
                param.Add("@PI_SendoSupportFee", claimModel.SendoSupportFee, DbType.Decimal);
                param.Add("@PI_SellerShippingFee", claimModel.SellerShippingFee, DbType.Decimal);

                param.Add("@PI_User", username, size: 50);
                param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);
                param.Add("@PO_Id", dbType: DbType.Int32, direction: ParameterDirection.Output);

                //Thực thi store procedure (insert/update/delete)
                string errorMessage = string.Empty;
                int claimId = 0;
                int rowCount = DBConnection.ExecuteStoreProcedureReturnId("sp_CreateClaim", out errorMessage, out claimId, param);

                if (rowCount > 0 && claimId > 0)
                {
                    //Get thông tin chi tiết khiếu nại
                    ResultData claimInfoResult = GetClaimById(claimId);
                    if (claimInfoResult.IsError == false)
                    {
                        return new ResultData(isError: true, message: "Tạo khiếu nại thành công!", totalCount: 1, data: claimInfoResult.Data);
                    }
                    else
                    {
                        return new ResultData(isError: true, message: claimInfoResult.Message, data: null);
                    }
                }
                else
                {
                    return new ResultData(isError: true, message: errorMessage, data: null);
                }
            }
            //Trường hợp đóng băng đơn hàng không thành công
            else
            {
                return new ResultData(isError: true, message: freezeOrderResult.Message, totalCount: 0, data: null);
            }
        }

        /// <summary>
        /// 03 API get chi tiết khiếu nại theo OrderId
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public ResultData GetClaimByOrderId(int orderId)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_OrderId", orderId, DbType.Int32);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure get danh sách khiếu nại theo salesOrderId
            string errorMessage = string.Empty;
            List<ClaimDetailModel> claimEvidences = DBConnection.QueryByStoreProcedure<ClaimDetailModel>("sp_GetClaimByOrderId", out errorMessage, param);

            if (null != claimEvidences && string.IsNullOrEmpty(errorMessage) && claimEvidences.Count > 0)
            {
                ClaimModel claimModel = new ClaimModel();

                //Set thông tin khiếu nại
                claimModel.ClaimId = claimEvidences[0].ClaimId;
                claimModel.ClaimNo = claimEvidences[0].ClaimNo;
                claimModel.ClaimStatus = claimEvidences[0].ClaimStatus;
                claimModel.ClaimStatusDate = claimEvidences[0].ClaimStatusDate;
                claimModel.ClaimType = claimEvidences[0].ClaimType;
                claimModel.Reason = claimEvidences[0].Reason;
                claimModel.ReasonDetail = claimEvidences[0].ReasonDetail;

                claimModel.OrderId = claimEvidences[0].OrderId;
                claimModel.OrderNumber = claimEvidences[0].OrderNumber;
                claimModel.OrderDate = claimEvidences[0].OrderDate;
                claimModel.TotalPayment = claimEvidences[0].TotalPayment;
                claimModel.BuyerId = claimEvidences[0].BuyerId;
                claimModel.BuyerName = claimEvidences[0].BuyerName;
                claimModel.BuyerPhone = claimEvidences[0].BuyerPhone;
                claimModel.BuyerEmail = claimEvidences[0].BuyerEmail;
                claimModel.StoreId = claimEvidences[0].StoreId;
                claimModel.StoreName = claimEvidences[0].StoreName;
                claimModel.StorePhone = claimEvidences[0].StorePhone;
                claimModel.StoreEmail = claimEvidences[0].StoreEmail;
                claimModel.CarrierName = claimEvidences[0].CarrierName;
                claimModel.DiscountAmount = claimEvidences[0].DiscountAmount;
                claimModel.ShippingFee = claimEvidences[0].ShippingFee;
                claimModel.SendoSupportFee = claimEvidences[0].SendoSupportFee;
                claimModel.SellerShippingFee = claimEvidences[0].SellerShippingFee;

                claimModel.BuyerNote = claimEvidences[0].BuyerNote;
                claimModel.CSNote = claimEvidences[0].CSNote;
                claimModel.SellerNote = claimEvidences[0].SellerNote;
                claimModel.CreatedDate = claimEvidences[0].CreatedDate;
                claimModel.UpdatedUser = claimEvidences[0].UpdatedUser;
                claimModel.UpdatedDate = claimEvidences[0].UpdatedDate;

                //Set thông tin phán quyết của khiếu nại
                claimModel.JudgementId = claimEvidences[0].JudgementId;
                claimModel.JudgementStatus = claimEvidences[0].JudgementStatus;
                claimModel.JudgementType = claimEvidences[0].JudgementType;
                claimModel.JudgementContent = claimEvidences[0].JudgementContent;
                claimModel.BuyerRefundAmount = claimEvidences[0].BuyerRefundAmount;
                claimModel.SellerRefundAmount = claimEvidences[0].SellerRefundAmount;
                claimModel.IsSellerViolation = claimEvidences[0].IsSellerViolation;

                //Set thông tin bằng chứng khiếu nại
                if (claimEvidences.Count == 1 && claimEvidences[0].ClaimEvidenceId == 0)
                {
                    claimModel.ClaimEvidences = null;
                }
                else
                {
                    claimModel.ClaimEvidences = claimEvidences;
                }

                return new ResultData(isError: false, message: string.Empty, totalCount: 1, data: claimModel);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        /// <summary>
        /// 04 API get danh sách khiếu nại theo BuyerId (ClaimStatus = 10,11,12)
        /// </summary>
        /// <param name="buyerId"></param>
        /// <returns></returns>
        public ResultData GetClaimListByBuyerId_TabCompleted(int buyerId)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_BuyerId", buyerId, DbType.Int32);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure get danh sách khiếu nại theo salesOrderId
            string errorMessage = string.Empty;
            List<ClaimModel> listClaim = DBConnection.QueryByStoreProcedure<ClaimModel>("sp_GetClaimListByBuyerId_TabCompleted", out errorMessage, param);

            if (null != listClaim && string.IsNullOrEmpty(errorMessage))
            {
                return new ResultData(isError: false, message: string.Empty, totalCount: listClaim.Count, data: listClaim);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        /// <summary>
        /// 05 API get danh sách đơn hàng theo BuyerId (Đã giao hàng và trong 48H)
        /// </summary>
        /// <param name="buyerId"></param>
        /// <returns></returns>
        public ResultData GetOrderListByBuyerId_POD_48H(int buyerId)
        {
            try
            {
                //Tạo paramter
                var param = new
                {
                    BuyerId = buyerId,
                    OrderStatus = new List<int>() { 7 },
                    //FromDate = DateTime.Now.AddDays(-2),
                    FromDate = DateTime.Now.AddDays(-60),
                    ToDate = DateTime.Now,
                    PageSize = 100,
                    PageNumber = 1
                };

                string requestAddress = "http://api-opc.test.sendo.vn/api/SalesOrder/searchorder";

                //Gọi Api bằng phương thức Post
                string responseString = Utils.OpcPostJson(requestAddress, param);

                if (!string.IsNullOrEmpty(responseString))
                {
                    //Deserialize từ json data thành object
                    OpcResultData responseResult = JsonConvert.DeserializeObject<OpcResultData>(responseString);

                    //Trường hợp gọi API thành công
                    if (responseResult.Status == 200)
                    {
                        OpcResultData resultData = JsonConvert.DeserializeObject<OpcResultData>(responseResult.Data.ToString());

                        //Convert data thành object
                        JArray jsonArrayData = (JArray)resultData.Results;
                        List<OrderModel> listOrder = jsonArrayData.ToObject<List<OrderModel>>();

                        return new ResultData(isError: false, message: string.Empty, totalCount: resultData.TotalRecords, data: listOrder);
                    }
                    else
                    {
                        return new ResultData(isError: true, message: responseResult.Message, totalCount: 0, data: null);
                    }
                }
                else
                {
                    return new ResultData(isError: true, message: "Call api/searchorder error!", totalCount: 0, data: null);
                }
            }
            catch (Exception ex)
            {
                return new ResultData(isError: true, message: ex.Message, totalCount: 0, data: null);
            }
        }

        /// <summary>
        /// 06 API get danh sách khiếu nại theo BuyerId (ClaimStatus = 1 -> 9)
        /// </summary>
        /// <param name="buyerId"></param>
        /// <returns></returns>
        public ResultData GetClaimListByBuyerId_TabProcessing(int buyerId)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_BuyerId", buyerId, DbType.Int32);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure get danh sách khiếu nại theo salesOrderId
            string errorMessage = string.Empty;
            List<ClaimModel> listClaim = DBConnection.QueryByStoreProcedure<ClaimModel>("sp_GetClaimListByBuyerId_TabProcessing", out errorMessage, param);

            if (null != listClaim && string.IsNullOrEmpty(errorMessage))
            {
                return new ResultData(isError: false, message: string.Empty, totalCount: listClaim.Count, data: listClaim);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        #endregion

        #region Complaint Send Email

        private void SendEmailByClaimStatus(int claimId, int claimStatus)
        {
            //Get thông tin chi tiết khiếu nại
            ResultData claimInfoResult = GetClaimById(claimId);
            if (claimInfoResult.IsError == false)
            {
                ClaimModel claimInfo = (ClaimModel)claimInfoResult.Data;

                switch (claimStatus)
                {
                    case (int)ClaimStatus.CS_REQUEST_UPDATE:
                        SendEmailByTemplate(claimInfo, TO_BUYER_CLAIM_REQUEST_UPDATE);
                        break;
                    case (int)ClaimStatus.CANCEL:
                        SendEmailByTemplate(claimInfo, TO_BUYER_CLAIM_INFO_CANCEL);
                        break;
                    case (int)ClaimStatus.CS_REQUEST_NEGOTIATE:
                        //Thông báo cho buyer CS đã tiếp nhận khiếu nại
                        SendEmailByTemplate(claimInfo, TO_BUYER_CLAIM_INFO_ACCEPT);
                        //Thông báo cho shop CS đã tiếp nhận khiếu nại và yêu cầu shop thỏa thuận với buyer
                        SendEmailByTemplate(claimInfo, TO_SELLER_CLAIM_INFO_ACCEPT);
                        break;
                }
            }
        }

        private void  SendEmailByTemplate(ClaimModel claimInfo, string templateName)
        {
            EmailModel emailModel = new EmailModel();

            //Email from to
            emailModel.From = new EmailAddress() { Name = "Sendo.vn", Email = "hotro@sendo.vn" };
            emailModel.To = new List<EmailAddress>() { new EmailAddress() { Name = claimInfo.BuyerName, Email = "tienhv3@sendo.vn" } };

            //Template Name
            emailModel.TemplateName = templateName;

            //Tủy email template sẽ set lại subject và email to
            switch (templateName)
            {
                case TO_BUYER_CLAIM_INFO_NEW:
                    emailModel.Subject = "SENDO ĐÃ NHẬN ĐƯỢC YÊU CẦU CỦA BẠN";
                    break;
                case TO_BUYER_CLAIM_REQUEST_UPDATE:
                    emailModel.Subject = "SENDO CẦN THÊM THÔNG TIN CHO YÊU CẦU ĐỔI/TRẢ";
                    break;
                case TO_BUYER_CLAIM_INFO_CANCEL:
                    emailModel.Subject = "THÔNG BÁO TỪ CHỐI TIẾP NHẬN YÊU CẦU ĐỔI/TRẢ";
                    break;
                case TO_BUYER_CLAIM_INFO_ACCEPT:
                    emailModel.Subject = "THÔNG BÁO TIẾP NHẬN YÊU CẦU ĐỔI/TRẢ";
                    break;
                case TO_SELLER_CLAIM_INFO_ACCEPT:
                    //Set lại email from to
                    emailModel.From = new EmailAddress() { Name = "Sendo.vn", Email = "hotronguoiban@sendo.vn" };
                    emailModel.To = new List<EmailAddress>() { new EmailAddress() { Name = claimInfo.StoreName, Email = "tienhv3@sendo.vn" } };

                    emailModel.Subject = "THÔNG BÁO CÓ KHIẾU NẠI PHÁT SINH";
                    break;
            }

            emailModel.EmailParams = new List<EmailParam>()
            {
                new EmailParam() { Name = "BuyerName", Value = claimInfo.BuyerName },
                new EmailParam() { Name = "BuyerPhone", Value = claimInfo.BuyerPhone },
                new EmailParam() { Name = "BuyerEmail", Value = claimInfo.BuyerEmail },
                new EmailParam() { Name = "ClaimNo", Value = claimInfo.ClaimNo },
                new EmailParam() { Name = "ClaimDate", Value = claimInfo.CreatedDate },
                new EmailParam() { Name = "ClaimReason", Value = Utils.GetAttributeNameOfEnum(typeof(ClaimReason), claimInfo.Reason) },
                new EmailParam() { Name = "ClaimReasonDetail", Value = claimInfo.ReasonDetail },
                new EmailParam() { Name = "OrderNumber", Value = claimInfo.OrderNumber },
                new EmailParam() { Name = "OrderDate", Value = claimInfo.OrderDate.ToString() },
                new EmailParam() { Name = "ShopName", Value = claimInfo.StoreName },
                new EmailParam() { Name = "ShopPhone", Value = claimInfo.StorePhone }

                //TODO
                //Còn xử lý gọi api get danh sách sản phẩm đơn hàng để bind vào email tempate
            };

            //Send mail
            Task.Run(async () => await EmailService.Send(emailModel));
        }

        #endregion

        #region OPC

        public ResultData OpcGetClaimByOrderId(int orderId)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_OrderId", orderId, DbType.Int32);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure get danh sách khiếu nại theo salesOrderId
            string errorMessage = string.Empty;
            List<ClaimDetailModel> claimEvidences = DBConnection.QueryByStoreProcedure<ClaimDetailModel>("sp_GetClaimByOrderId", out errorMessage, param);

            if (null != claimEvidences && string.IsNullOrEmpty(errorMessage) && claimEvidences.Count > 0)
            {
                //Set thông tin khiếu nại
                var returnModel = new
                {
                    ClaimId = claimEvidences[0].ClaimId,
                    ClaimNo = claimEvidences[0].ClaimNo,
                    ClaimDate = claimEvidences[0].CreatedDate,
                    ClaimStatus = claimEvidences[0].ClaimStatus,
                    ClaimStatusDate = claimEvidences[0].ClaimStatusDate,
                    ClaimType = claimEvidences[0].ClaimType,
                    Reason = claimEvidences[0].Reason,
                    ReasonDetail = claimEvidences[0].ReasonDetail
                };

                return new ResultData(isError: false, message: string.Empty, totalCount: 1, data: returnModel);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        #endregion

    }
}