﻿using Dapper;
using InsideBMC.API.Models;
using InsideBMC.API.Models.Order;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace InsideBMC.API.Services
{
    public class OrderService
    {
        /// <summary>
        /// Get chi tiết đơn hàng theo Id
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public ResultData GetOrderDetail(int orderId)
        {
            try
            {
                //OPC API address
                string requestAddress = string.Format("http://api-opc.test.sendo.vn/api/SalesOrder/getorderdetail/{0}", orderId);

                //Gọi Api bằng phương thức Post
                string responseString = Utils.OpcGetJson(requestAddress);

                if (!string.IsNullOrEmpty(responseString))
                {
                    //Deserialize từ json data thành object
                    OpcResultData responseResult = JsonConvert.DeserializeObject<OpcResultData>(responseString);

                    //Trường hợp gọi API thành công
                    if (responseResult.Status == 200)
                    {
                        OpcResultData dataResult = JsonConvert.DeserializeObject<OpcResultData>(JsonConvert.SerializeObject(responseResult.Data));

                        //Convert data ListProduct thành Object
                        JObject jsonData = (JObject)dataResult.OrderDetail;
                        OrderDetail orderDetail = jsonData.ToObject<OrderDetail>();

                        return new ResultData(isError: false, message: string.Empty, totalCount: 1, data: orderDetail);
                    }
                    else
                    {
                        return new ResultData(isError: true, message: responseResult.Message, totalCount: 0, data: null);
                    }
                }
                else
                {
                    return new ResultData(isError: true, message: "Call api/getorderdetai/{id} error!", totalCount: 0, data: null);
                }
            }
            catch (Exception ex)
            {
                return new ResultData(isError: true, message: ex.Message, totalCount: 0, data: null);
            }
        }

        public async Task<ResultData> CalculateDeclareValueFee(string carrierCode, decimal subTotal, decimal shippingFee, decimal totalAmount)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    //tạo param
                    var param = new
                    {
                        carriercode = carrierCode,
                        subtotal = subTotal,
                        shippingFee = shippingFee,
                        codAmount = totalAmount
                    };

                    client.BaseAddress = new Uri("http://localapi.test.sendo.vn/api/");
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Gọi Api bằng phương thức POST  
                    HttpResponseMessage httpResponseMessage = await client.PostAsync("SalesOrder/CalculateDeclareValueFee", new StringContent(JsonConvert.SerializeObject(param).ToString(), Encoding.UTF8, "application/json"));

                    //Kiểm tra trạng thái gọi API có thành công hay không
                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        //Get giá trị trả về từ API
                        var result = httpResponseMessage.Content.ReadAsStringAsync().Result;

                        DeclareValueFeeModel model = JsonConvert.DeserializeObject<DeclareValueFeeModel>(result);
                        decimal declareValueFee = JsonConvert.DeserializeObject<DeclareValueFeeModel>(JsonConvert.SerializeObject(model.result)).data;

                        return new ResultData(isError: false, message: string.Empty, totalCount: 1, data: declareValueFee);
                    }
                    else
                    {
                        return new ResultData(isError: true, message: "Status code: " + httpResponseMessage.StatusCode, data: null);
                    }
                }
            }
            catch (Exception ex)
            {
                return new ResultData(isError: true, message: ex.Message, data: null);
            }
        }

        public ResultData GetExchangeOrderNumber(string orderParentNumber, int orderParentId, int exchangeType)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_OrderParentNumber", orderParentNumber, DbType.String, size: 50);
            param.Add("@PI_OrderParentId", orderParentId, DbType.Int32);
            param.Add("@PI_ExchangeType", exchangeType, DbType.Int32);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);
            string errorMessage = string.Empty;
            string exchageOrderNumber = DBConnection.QueryByStoreProcedure<string>("sp_GetExchangeOrderNumber", out errorMessage, param).FirstOrDefault();

            if (!string.IsNullOrEmpty(exchageOrderNumber))
            {
                return new ResultData(isError: false, message: string.Empty, totalCount: 1, data: exchageOrderNumber);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        public ResultData CreateExchangeOrder(ExchangeOrderModel exchangeOrderModel)
        {
            //Tạo parameter
            var param = new DynamicParameters();
            param.Add("@PI_OrderNumber", exchangeOrderModel.OrderNumber, size: 50);
            param.Add("@PI_OrderParentId", exchangeOrderModel.OrderParentId, DbType.Int32);
            param.Add("@PI_ExchangeType", exchangeOrderModel.ExchangeType, DbType.Int32);
            param.Add("@PI_BuyerFee", exchangeOrderModel.BuyerFee, DbType.Decimal);
            param.Add("@PI_SellerFee", exchangeOrderModel.SellerFee, DbType.Decimal);
            param.Add("@PI_SendoFee", exchangeOrderModel.SendoFee, DbType.Decimal);
            param.Add("@PI_CODFee", exchangeOrderModel.CODFee, DbType.Decimal);
            param.Add("@PI_CODBuyerFee", exchangeOrderModel.CODBuyerFee, DbType.Decimal);
            param.Add("@PI_CODSellerFee", exchangeOrderModel.CODSellerFee, DbType.Decimal);
            param.Add("@PI_CODSendoFee", exchangeOrderModel.CODSendoFee, DbType.Decimal);
            param.Add("@PI_FromName", exchangeOrderModel.FromName, size: 100);
            param.Add("@PI_FromPhone", exchangeOrderModel.FromPhone, size: 50);
            param.Add("@PI_FromAddress", exchangeOrderModel.FromAddress, size: 400);
            param.Add("@PI_ToName", exchangeOrderModel.ToName, size: 100);
            param.Add("@PI_ToPhone", exchangeOrderModel.ToPhone, size: 50);
            param.Add("@PI_ToAddress", exchangeOrderModel.ToAddress, size: 400);
            param.Add("@PI_FromWardId", exchangeOrderModel.FromWardId, DbType.Int32);
            param.Add("@PI_FromDistrictId", exchangeOrderModel.FromDistrictId, DbType.Int32);
            param.Add("@PI_FromRegionId", exchangeOrderModel.FromRegionId, DbType.Int32);
            param.Add("@PI_ToWardId", exchangeOrderModel.ToWardId, DbType.Int32);
            param.Add("@PI_ToDistrictId", exchangeOrderModel.ToDistrictId, DbType.Int32);
            param.Add("@PI_ToRegionId", exchangeOrderModel.ToRegionId, DbType.Int32);
            param.Add("@PI_Note", exchangeOrderModel.Note, size: 1000);
            param.Add("@PI_StoreId", exchangeOrderModel.StoreId, DbType.Int32);

            param.Add("@PI_User", exchangeOrderModel.UserName, size: 50);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            //Thực thi store procedure (insert/update/delete)
            string errorMessage = string.Empty;
            int rowCount = DBConnection.ExecuteCommandByStoreProcedure("sp_CreateOrderExchange", out errorMessage, param);

            if (rowCount > 0)
            {
                return new ResultData(isError: false, message: "Tạo đơn hàng đổi/trả/bổ sung thành công!", totalCount: 1, data: null);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        public ResultData SearchOrder(OrderSearchForm orderSearchForm)
        {
            try
            {
                object param = null;
                param = new
                {
                    OrderNumber = orderSearchForm.OrderNumber,
                    BuyerName = string.IsNullOrEmpty(orderSearchForm.BuyerName) ? string.Empty : orderSearchForm.BuyerName,
                    OrderStatus = orderSearchForm.OrderStatus > 0 ? new List<int> { orderSearchForm.OrderStatus } : new List<int> { },
                    //BuyerId = orderSearchForm.BuyerId == 0 ? null : (int?)orderSearchForm.BuyerId,
                    PaymentMethod = orderSearchForm.PaymentMethod == 0 ? null : (int?)orderSearchForm.PaymentMethod,
                    PaymentStatus = orderSearchForm.PaymentStatus > 0 ? new List<int> { orderSearchForm.PaymentStatus } : new List<int> { },
                    IsDisputing = orderSearchForm.IsDisputing,
                    ReceiverName = orderSearchForm.ReceiverName,
                    ShippingContactPhone = orderSearchForm.ShippingContactPhone,
                    CarrierIds = orderSearchForm.CarrierId > 0 ? new List<int> { orderSearchForm.CarrierId } : new List<int> { },
                    DeliveryStatus = orderSearchForm.CarrierStatus > 0 ? new List<int> { orderSearchForm.CarrierStatus } : new List<int> { },
                    ReasonCancelCode = string.IsNullOrEmpty(orderSearchForm.ReasonCancelCode) || orderSearchForm.ReasonCancelCode == "0" ? null : orderSearchForm.ReasonCancelCode,
                    FromDate = orderSearchForm.FromDate,
                    ToDate = orderSearchForm.ToDate,
                    PageNumber = orderSearchForm.PageNumber,
                    PageSize = orderSearchForm.PageSize
                };

                //OPC API address
                string requestAddress = "http://api-opc.test.sendo.vn/api/SalesOrder/searchorder";

                //Gọi Api bằng phương thức Post
                string responseString = Utils.OpcPostJson(requestAddress, param);

                if (!string.IsNullOrEmpty(responseString))
                {
                    //Deserialize từ json data thành object
                    OpcResultData responseResult = JsonConvert.DeserializeObject<OpcResultData>(responseString);

                    //Trường hợp gọi API thành công
                    if (responseResult.Status == 200)
                    {
                        OpcResultData resultData = JsonConvert.DeserializeObject<OpcResultData>(responseResult.Data.ToString());

                        //Convert data thành object
                        JArray jsonArrayData = (JArray)resultData.Results;
                        List<OrderModel> listOrder = jsonArrayData.ToObject<List<OrderModel>>();

                        return new ResultData(isError: false, message: string.Empty, totalCount: resultData.TotalRecords, data: listOrder);
                    }
                    else
                    {
                        return new ResultData(isError: true, message: responseResult.Message, totalCount: 0, data: null);
                    }
                }
                else
                {
                    return new ResultData(isError: true, message: "Call api/searchorder error!", totalCount: 0, data: null);
                }
            }
            catch (Exception ex)
            {
                return new ResultData(isError: true, message: ex.Message, totalCount: 0, data: null);
            }
        }

        public ResultData GetListPaymentByOrderId(int orderId)
        {
            try
            {
                //OPC API address
                string requestAddress = string.Format("http://api-opc.test.sendo.vn/api/SalesOrder/getorderdetail/{0}", orderId);

                //Gọi Api bằng phương thức Post
                string responseString = Utils.OpcGetJson(requestAddress);

                if (!string.IsNullOrEmpty(responseString))
                {
                    //Deserialize từ json data thành object
                    OpcResultData responseResult = JsonConvert.DeserializeObject<OpcResultData>(responseString);

                    //Trường hợp gọi API thành công
                    if (responseResult.Status == 200)
                    {
                        OpcResultData dataResult = JsonConvert.DeserializeObject<OpcResultData>(JsonConvert.SerializeObject(responseResult.Data));

                        //Convert data ListProduct thành Object
                        JArray jsonArrayData = (JArray)dataResult.ListPayment;
                        List<PaymentModel> listPayment = jsonArrayData.ToObject<List<PaymentModel>>();

                        return new ResultData(isError: false, message: string.Empty, totalCount: listPayment.Count, data: listPayment);
                    }
                    else
                    {
                        return new ResultData(isError: true, message: responseResult.Message, totalCount: 0, data: null);
                    }
                }
                else
                {
                    return new ResultData(isError: true, message: "Call api/getorderdetail/{id} error!", totalCount: 0, data: null);
                }
            }
            catch (Exception ex)
            {
                return new ResultData(isError: true, message: ex.Message, totalCount: 0, data: null);
            }
        }

        public ResultData GetListStatusHistoryByOrderId(int orderId)
        {
            try
            {
                //OPC API address
                string requestAddress = string.Format("http://api-opc.test.sendo.vn/api/SalesOrder/getorderdetail/{0}", orderId);

                //Gọi Api bằng phương thức Post
                string responseString = Utils.OpcGetJson(requestAddress);

                if (!string.IsNullOrEmpty(responseString))
                {
                    //Deserialize từ json data thành object
                    OpcResultData responseResult = JsonConvert.DeserializeObject<OpcResultData>(responseString);

                    //Trường hợp gọi API thành công
                    if (responseResult.Status == 200)
                    {
                        OpcResultData dataResult = JsonConvert.DeserializeObject<OpcResultData>(JsonConvert.SerializeObject(responseResult.Data));

                        //Convert data ListProduct thành Object
                        JArray jsonArrayData = (JArray)dataResult.ListStatusHistory;
                        List<StatusHistoryModel> listStatusHistory = jsonArrayData.ToObject<List<StatusHistoryModel>>();

                        return new ResultData(isError: false, message: string.Empty, totalCount: listStatusHistory.Count, data: listStatusHistory);
                    }
                    else
                    {
                        return new ResultData(isError: true, message: responseResult.Message, totalCount: 0, data: null);
                    }
                }
                else
                {
                    return new ResultData(isError: true, message: "Call api/getorderdetail/{id} error!", totalCount: 0, data: null);
                }
            }
            catch (Exception ex)
            {
                return new ResultData(isError: true, message: ex.Message, totalCount: 0, data: null);
            }
        }

        public ResultData GetExchangeOrderByOrderId(int orderId)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_ParentOrderId", orderId, DbType.Int32);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);
            string errorMessage = string.Empty;
            List<ExchangeOrderModel> listExchangeOrder = DBConnection.QueryByStoreProcedure<ExchangeOrderModel>("sp_GetExchangeOrderByOrderId", out errorMessage, param);

            if (listExchangeOrder != null && listExchangeOrder.Count > 0)
            {
                return new ResultData(isError: false, message: string.Empty, totalCount: listExchangeOrder.Count, data: listExchangeOrder);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        public ResultData CancelOrder(CancelOrderForm cancelOrderForm)
        {
            try
            {
                //OPC API address
                string requestAddress = "http://api-opc.test.sendo.vn/api/SalesOrder/cancelorder";

                //Gọi Api bằng phương thức Post
                string responseString = Utils.OpcPostJson(requestAddress, cancelOrderForm);

                if (!string.IsNullOrEmpty(responseString))
                {
                    //Deserialize từ json data thành object
                    OpcResultData responseResult = JsonConvert.DeserializeObject<OpcResultData>(responseString);

                    //Trường hợp gọi API thành công
                    if (responseResult.Status == 200)
                    {
                        return new ResultData(isError: false, message: string.Empty, data: true);
                    }
                    else
                    {
                        return new ResultData(isError: true, message: responseResult.Message, totalCount: 0, data: null);
                    }
                }
                else
                {
                    return new ResultData(isError: true, message: "Call api/cancelorder error!", totalCount: 0, data: null);
                }
            }
            catch (Exception ex)
            {
                return new ResultData(isError: true, message: ex.Message, totalCount: 0, data: null);
            }
        }

        /// <summary>
        /// API gọi OPC đóng băng đơn hàng
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns></returns>
        public ResultData FreezeOrder(string orderNumber, string username)
        {
            try
            {
                var param = new
                {
                    OrderNumber = orderNumber,
                    CreatedUser = username
                };

                //API Adress
                string requestAddress = "http://api-opc.test.sendo.vn/api/SalesOrder/freezeorder";

                //Gọi Api bằng phương thức Post
                string responseString = Utils.OpcPostJson(requestAddress, param);

                if (!string.IsNullOrEmpty(responseString))
                {
                    //Deserialize từ json data thành object
                    OpcResultData responseResult = JsonConvert.DeserializeObject<OpcResultData>(responseString);

                    //Trường hợp gọi API thành công
                    if (responseResult.Status == 200)
                    {
                        return new ResultData(isError: false, message: string.Empty, totalCount: 1, data: responseResult.Data);
                    }
                    else
                    {
                        return new ResultData(isError: true, message: responseResult.Message, totalCount: 0, data: null);
                    }
                }
                else
                {
                    return new ResultData(isError: true, message: "Call api/freezeorder error!", totalCount: 0, data: null);
                }
            }
            catch (Exception ex)
            {
                return new ResultData(isError: true, message: ex.Message, totalCount: 0, data: null);
            }
        }

        public ResultData GetListOrderByBuyerId(OrderSearchForm orderSearchForm)
        {
            try
            {
                //Tạo paramter
                var param = new
                {
                    BuyerId = orderSearchForm.BuyerId,
                    FromDate = orderSearchForm.FromDate,
                    ToDate = orderSearchForm.ToDate,
                    PageSize = orderSearchForm.PageSize,
                    PageNumber = orderSearchForm.PageNumber
                };

                string requestAddress = "http://api-opc.test.sendo.vn/api/SalesOrder/searchorder";

                //Gọi Api bằng phương thức Post
                string responseString = Utils.OpcPostJson(requestAddress, param);

                if (!string.IsNullOrEmpty(responseString))
                {
                    //Deserialize từ json data thành object
                    OpcResultData responseResult = JsonConvert.DeserializeObject<OpcResultData>(responseString);

                    //Trường hợp gọi API thành công
                    if (responseResult.Status == 200)
                    {
                        OpcResultData resultData = JsonConvert.DeserializeObject<OpcResultData>(responseResult.Data.ToString());

                        //Convert data thành object
                        JArray jsonArrayData = (JArray)resultData.Results;
                        List<OrderModel> listOrder = jsonArrayData.ToObject<List<OrderModel>>();

                        return new ResultData(isError: false, message: string.Empty, totalCount: resultData.TotalRecords, data: listOrder);
                    }
                    else
                    {
                        return new ResultData(isError: true, message: responseResult.Message, totalCount: 0, data: null);
                    }
                }
                else
                {
                    return new ResultData(isError: true, message: "Call api/searchorder error!", totalCount: 0, data: null);
                }
            }
            catch (Exception ex)
            {
                return new ResultData(isError: true, message: ex.Message, totalCount: 0, data: null);
            }
        }
    }
}