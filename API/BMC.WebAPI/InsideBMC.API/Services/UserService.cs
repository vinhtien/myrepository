﻿using Dapper;
using InsideBMC.API.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace InsideBMC.API.Services
{
    public class UserService
    {
        /// <summary>
        /// Lấy thông tin user login
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="errorMessage">message lỗi</param>
        /// <returns></returns>
        public UserModel GetUserByCredentials(string username, string password, out string errorMessage)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_UserName", username,size: 50);
            param.Add("@PI_Password", password, size: 20);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            List<UserModel> result = DBConnection.QueryByStoreProcedure<UserModel>("sp_GetUserLogin", out errorMessage, param);

            if (null != result && result.Count > 0)
            {
                return result.FirstOrDefault();
            }
            return null;
        }
    }
}