﻿using Dapper;
using InsideBMC.API.Models;
using InsideBMC.API.Models.Region;
using System.Collections.Generic;
using System.Data;

namespace InsideBMC.API.Services
{
    public class RegionService
    {
        public ResultData GetRegion()
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            string errorMessage = string.Empty;
            List<RegionModel> listRegion = DBConnection.QueryByStoreProcedure<RegionModel>("sp_GetRegion", out errorMessage, param);

            if (null != listRegion)
            {
                return new ResultData(isError: false, message: string.Empty, totalCount:listRegion.Count, data: listRegion);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        public ResultData GetDistrict()
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            string errorMessage = string.Empty;
            List<DistrictModel> listDistrict = DBConnection.QueryByStoreProcedure<DistrictModel>("sp_GetDistrict", out errorMessage, param);

            if (null != listDistrict)
            {
                return new ResultData(isError: false, message: string.Empty, totalCount: listDistrict.Count, data: listDistrict);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        public ResultData GetDistrictByRegion(int regionId)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_RegionId", regionId, dbType: DbType.Int32);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            string errorMessage = string.Empty;
            List<DistrictModel> listDistrict = DBConnection.QueryByStoreProcedure<DistrictModel>("sp_GetDistrictByRegion", out errorMessage, param);

            if (null != listDistrict)
            {
                return new ResultData(isError: false, message: string.Empty, totalCount: listDistrict.Count, data: listDistrict);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        public ResultData GetWardByDistrict(int districtId)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_DistrictId", districtId, dbType: DbType.Int32);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            string errorMessage = string.Empty;
            List<WardModel> listWard = DBConnection.QueryByStoreProcedure<WardModel>("sp_GetWardByDistrict", out errorMessage, param);

            if (null != listWard)
            {
                return new ResultData(isError: false, message: string.Empty, totalCount: listWard.Count, data: listWard);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }

        public ResultData GetRegionData(int regionId, int districtId)
        {
            //Tạo parameter truyền vào store
            var param = new DynamicParameters();
            param.Add("@PI_RegionId", regionId, dbType: DbType.Int32);
            param.Add("@PI_DistrictId", districtId, dbType: DbType.Int32);
            param.Add("@PO_ErrorMessage", direction: ParameterDirection.Output, size: 100);

            string errorMessage = string.Empty;
            RegionDataModel result = DBConnection.QueryRegionData("sp_GetRegionData", out errorMessage, param);

            if (null != result)
            {
                return new ResultData(isError: false, message: string.Empty, data: result);
            }
            else
            {
                return new ResultData(isError: true, message: errorMessage, data: null);
            }
        }
    }
}