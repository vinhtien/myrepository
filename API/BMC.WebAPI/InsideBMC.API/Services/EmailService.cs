﻿using InsideBMC.API.Models;
using InsideBMC.API.Models.Email;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace InsideBMC.API.Services
{
    public static class EmailService
    {
        public static async Task<ResultData> Send(EmailModel model)
        {
            //Data test
            /*----------------------------------------------------------------------------------------------------------*/
            //var param = new EmailModel();
            //param.From = new EmailAddress() { Name = "tien from", Email = "tienhv3@sendo.vn" };
            //param.To = new List<EmailAddress>() { new EmailAddress() { Name = "tien to", Email = "tienhv3@sendo.vn" } };
            //param.Subject = "test send email";
            //param.TemplateName = "PrivateOffers.Buyer";
            //param.EmailParams = new List<EmailParam>() { new EmailParam() { Name = "abc", Value = "123" } };
            /*----------------------------------------------------------------------------------------------------------*/

            using (var client = new HttpClient())
            {
                Uri requesetUri = new Uri("http://test.bmc-sendmail.sendo.vn/api/email/sendemail");
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Gọi Api bằng phương thức POST  
                HttpResponseMessage httpResponseMessage = await client.PostAsync(requesetUri, new StringContent(JsonConvert.SerializeObject(model).ToString(), Encoding.UTF8, "application/json"));

                //Kiểm tra trạng thái gọi API có thành công hay không
                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    //Get giá trị trả về từ API
                    var result = httpResponseMessage.Content.ReadAsStringAsync().Result;

                    //Deserialize từ json data thành object
                    EmailResultData emailResultData = JsonConvert.DeserializeObject<EmailResultData>(result);
                    if (emailResultData.Data == null || emailResultData.StatusCode != 200)
                    {
                        return new ResultData(isError: true, message: emailResultData.Message + " StatusCode: " + emailResultData.StatusCode, totalCount: 0, data: null);
                    }
                    return new ResultData(isError: false, message: emailResultData.Message, totalCount: 1, data: emailResultData.Data);
                }
                else
                {
                    return new ResultData(isError: true, message: "Gọi api sendemail lỗi! StatusCode: " + httpResponseMessage.StatusCode, data: null);
                }
            }

        }

    }
}