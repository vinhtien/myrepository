﻿using InsideBMC.API.Models;
using InsideBMC.API.Models.Store;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace InsideBMC.API.Services
{
    public class StoreService
    {
        public async Task<ResultData> GetStore(int id)
        {
            using (var client = new HttpClient())
            {
                string requestAddress = string.Format("http://service-smc.test.sendo.vn/department/store/{0}", id);
                Uri requesetUri = new Uri(requestAddress);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Gọi Api bằng phương thức POST  
                HttpResponseMessage httpResponseMessage = await client.GetAsync(requesetUri);

                //Kiểm tra trạng thái gọi API có thành công hay không
                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    //Get giá trị trả về từ API
                    var result = httpResponseMessage.Content.ReadAsStringAsync().Result;

                    //Deserialize từ json data thành object
                    SmcResultData smcResultData = JsonConvert.DeserializeObject<SmcResultData>(result);
                    if(smcResultData.result == null)
                    {
                        return new ResultData(isError: true, message: "Không có tồn tại StoreId này", totalCount: 0, data: null);
                    }
                    return new ResultData(isError: false, message: smcResultData.error.message, totalCount: 1, data: smcResultData.result);
                }
                else
                {
                    return new ResultData(isError: true, message: "Status code: " + httpResponseMessage.StatusCode, data: null);
                }
            }
        }
    }
}