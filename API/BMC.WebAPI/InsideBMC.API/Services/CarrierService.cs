﻿using InsideBMC.API.Models;
using InsideBMC.API.Models.Carrier;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace InsideBMC.API.Services
{
    public class CarrierService
    {
        public async Task<ResultData> GetShippingFee(int storeId,
                                                     int fromRegionId,
                                                     int toRegionId,
                                                     int fromDistrictId,
                                                     int toDistrictId,
                                                     int fromWardId,
                                                     int toWardId,
                                                     decimal actualWeight,
                                                     decimal subTotal,
                                                     int carrierId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    //Set Api Base Url
                    client.BaseAddress = new Uri("http://bmc-service.test.sendo.vn/");
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Gọi Api bằng phương thức Get
                    HttpResponseMessage httpResponseMessage = 
                        await client.GetAsync(String.Format("service/shipping/get-shippingfee-4/{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}/{8}/cod_payment/{9}/_/_/desktop/_",
                                                            storeId,
                                                            fromRegionId,
                                                            toRegionId,
                                                            fromDistrictId,
                                                            toDistrictId,
                                                            fromWardId,
                                                            toWardId,
                                                            actualWeight,
                                                            subTotal,
                                                            carrierId));

                    if (httpResponseMessage.IsSuccessStatusCode)
                    {
                        //Get giá trị trả về từ API
                        string result = await httpResponseMessage.Content.ReadAsStringAsync();

                        if (result == null || result == "null")
                        {
                            return new ResultData(isError: true, message: "Api tính phí vận chuyển trả về null!", totalCount: 0, data: null);
                        }

                        JArray jsonArrayData = JArray.Parse(result);

                        //Deserialize từ json data thành object
                        GetShippingFeeResponseModel responseResult = jsonArrayData.ToObject<List<GetShippingFeeResponseModel>>().FirstOrDefault();

                        return new ResultData(isError: false, message: string.Empty, totalCount: 1, data: responseResult);
                    }
                    else
                    {
                        return new ResultData(isError: true, message: "Call api tính phí vận chuyển thất bại!", totalCount: 0, data: null);
                    }
                }
            }
            catch (Exception ex)
            {
                return new ResultData(isError: true, message: ex.Message, totalCount: 0, data: null);
            }
        }

        public async Task<ResultData> GetCarriers()
        {
            using (var client = new HttpClient())
            {
                //Set Api Base Url
                client.BaseAddress = new Uri("http://bapi.test.sendo.vn/");
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Gọi Api bằng phương thức Get
                HttpResponseMessage httpResponseMessage = await client.GetAsync("shipping/get-carriers");

                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    //Get giá trị trả về từ API
                    string result = await httpResponseMessage.Content.ReadAsStringAsync();

                    JArray jsonArrayData = JArray.Parse(result);

                    return new ResultData(isError: false, message: string.Empty, totalCount: jsonArrayData.Count, data: jsonArrayData);
                }
                else
                {
                    return new ResultData(isError: true, message: "Call api get danh sách nhà vận chuyển thất bại!", totalCount: 0, data: null);
                }
            }
        }
    }
}