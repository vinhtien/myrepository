﻿using InsideBMC.API.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace InsideBMC.API.Services
{
    public class UploadService
    {
        #region Private Functions

        private string UploadFile(Stream file, string fileName)
        {
            fileName = ToAscii(fileName.Replace(" ", "_"), "_");
            fileName = fileName.Replace("simg", "_");

            var fileFormName = "file";
            var contenttype = "application/octet-stream";

            string serviceUrl = ConfigurationManager.AppSettings[ConfigKey.UPLOAD_SERVER_DOMAIN];
            Uri uri = new Uri(serviceUrl);

            string boundary = "----------" + DateTime.Now.Ticks.ToString("x");
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(uri);
            webrequest.ContentType = "multipart/form-data; boundary=" + boundary;
            webrequest.Method = "POST";

            // Build up the post message header
            StringBuilder sb = new StringBuilder();
            sb.Append("--");
            sb.Append(boundary);
            sb.Append("\r\n");
            sb.Append("Content-Disposition: form-data; name=\"");
            sb.Append(fileFormName);
            sb.Append("\"; filename=\"");
            sb.Append(fileName);
            sb.Append("\"");
            sb.Append("\r\n");
            sb.Append("Content-Type: ");
            sb.Append(contenttype);
            sb.Append("\r\n");
            sb.Append("\r\n");

            string postHeader = sb.ToString();

            byte[] postHeaderBytes = Encoding.UTF8.GetBytes(postHeader);

            // Build the trailing boundary string as a byte array
            // ensuring the boundary appears on a line by itself
            byte[] boundaryBytes = Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            long length = postHeaderBytes.Length + file.Length +
                                                   boundaryBytes.Length;
            webrequest.ContentLength = length;

            Stream requestStream = webrequest.GetRequestStream();

            // Write out our post header
            requestStream.Write(postHeaderBytes, 0, postHeaderBytes.Length);

            // Write out the file contents
            byte[] buffer = new Byte[checked((uint)Math.Min(4096,
                                     (int)file.Length))];
            int bytesRead = 0;
            while ((bytesRead = file.Read(buffer, 0, buffer.Length)) != 0)
                requestStream.Write(buffer, 0, bytesRead);

            // Write out the trailing boundary
            requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
            WebResponse responce = webrequest.GetResponse();
            Stream s = responce.GetResponseStream();
            StreamReader sr = new StreamReader(s);

            return sr.ReadToEnd();
        }

        private string ToAscii(string str, string replaceChar)
        {
            if (string.IsNullOrWhiteSpace(str)) return "";

            str = RemoveUnicodeCharacter(str);

            //Remove Special Char
            var regex = new Regex(@"[^a-zA-Z0-9_\.]");
            str = regex.Replace(str, replaceChar);

            return str;
        }

        private string RemoveUnicodeCharacter(string str)
        {
            var regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            str = str.Normalize(NormalizationForm.FormD);
            return regex.Replace(str, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        private string ValidateImageUpload(HttpPostedFile file)
        {
            //Check tên file
            string fileName = file.FileName;
            if (string.IsNullOrEmpty(fileName))
            {
                return "Tên file rỗng!";
            }

            //Check loại file 
            string fileExtension = Path.GetExtension(fileName);
            string[] imageExtensionAllow = ConfigurationManager.AppSettings[ConfigKey.IMAGE_EXTENSION_ALLOW].Split(',');
            if (!imageExtensionAllow.Any(x => fileExtension.ToLower().Contains(x)))
            {
                return "Loại file này không được hỗ trợ.";
            }

            //Check kích thước file
            Int32 imageUploadSize = Convert.ToInt32(ConfigurationManager.AppSettings[ConfigKey.IMAGE_UPLOAD_SIZE]);
            if ((file.ContentLength / (1024 * 1024)) > imageUploadSize)
            {
                return "File vượt quá giới hạn " + imageUploadSize.ToString() + " MB";
            }

            //Trả về kết quả không có lỗi
            return string.Empty;
        }

        #endregion

        #region Public Functions

        /// <summary>
        /// Thực hiện upload image
        /// </summary>
        /// <param name="file">file image</param>
        /// <returns></returns>
        public ResultData UploadImage(HttpPostedFile file)
        {
            //Thực hiện check file upload
            string errorMessage = ValidateImageUpload(file);

            if (!string.IsNullOrEmpty(errorMessage))
            {
                return new ResultData(isError: true, message: errorMessage);
            }

            try
            {
                //Thực hiện upload file
                string returnFilePath = UploadFile(file.InputStream, file.FileName);

                if (string.IsNullOrEmpty(returnFilePath))
                {
                    return new ResultData(isError: true, message: "Upload file không thành công!");
                }

                //Trường hợp thành công trả về ImageUrl và ImageName
                UploadImageResult result = new UploadImageResult();
                result.DocumentUrl = returnFilePath;
                result.DocumentName = file.FileName;
                return new ResultData(isError: false, message: string.Empty, data: result);
            }
            catch (Exception ex)
            {
                return new ResultData(isError: true, message: ex.Message);
            }
        }

        /// <summary>
        /// Upload image test lưu tại server của API
        /// </summary>
        /// <param name="file">file image</param>
        /// <returns></returns>
        public ResultData UploadImageTest(HttpPostedFile file)
        {
            try
            {
                var fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath("~/TestUpload"), file.FileName);

                // Lưu file upload vào folder "TestUpload"
                file.SaveAs(fileSavePath);

                string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                string applicationPath = HttpContext.Current.Request.ApplicationPath;

                if (applicationPath == "/")
                {
                    applicationPath = string.Empty;
                }

                string imageURL = domainName + applicationPath + "/TestUpload/" + file.FileName;

                UploadImageResult result = new UploadImageResult();
                result.DocumentUrl = imageURL;
                result.DocumentName = file.FileName;
                return new ResultData(isError: false, message: string.Empty, data: result);
            }
            catch (Exception ex)
            {
                return new ResultData(isError: true, message: ex.Message);
            }
        }

        public async Task<ResultData> UploadImageToAmazon(HttpPostedFile file)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    using (var formData = new MultipartFormDataContent())
                    {
                        formData.Add(new StreamContent(file.InputStream), "files", file.FileName);

                        string apiUploadDomain = ConfigurationManager.AppSettings[ConfigKey.API_UPLOAD_DOMAIN];

                        //Gọi Api bằng phương thức POST
                        HttpResponseMessage httpResponseMessage = await client.PostAsync(apiUploadDomain + "/upload/private", formData);

                        //Kiểm tra trạng thái gọi API có thành công hay không
                        if (httpResponseMessage.IsSuccessStatusCode)
                        {
                            //Get giá trị trả về từ API
                            var result = httpResponseMessage.Content.ReadAsStringAsync().Result;

                            UploadImageResult imageResult = JsonConvert.DeserializeObject<UploadImageResult>(result);

                            if (imageResult.status == true)
                            {
                                imageResult.DocumentName = file.FileName;
                                imageResult.DocumentUrl = apiUploadDomain + imageResult.filePath;
                                return new ResultData(isError: false, message: string.Empty, data: imageResult);
                            }
                            return new ResultData(isError: true, message: imageResult.error, data: null);
                        }
                        else
                        {
                            return new ResultData(isError: true, message: "Status code: " + httpResponseMessage.StatusCode, data: null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new ResultData(isError: true, message: ex.Message);
            }
        }

        #endregion
    }
}