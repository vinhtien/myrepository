﻿namespace InsideBMC.API.Models.Carrier
{
    public class GetShippingFeeResponseModel
    {
        public int CarrierId { get; set; }
        public string CarrierCode { get; set; }
        public string CarrierName { get; set; }
        public string CarrierLogoUrl { get; set; }
        public string CarrierPriceBoardUrl { get; set; }
        public string CarrierAbout { get; set; }
        public bool IsRequired { get; set; }
        public bool IsActive { get; set; }
        public int Position { get; set; }
        public bool IsArea { get; set; }
        public decimal Sort_wage { get; set; }
        public decimal Price { get; set; }
        public decimal Delivery_fee { get; set; }
        public decimal Cod_fee { get; set; }
        public decimal Original_fee { get; set; }
        public decimal Sub_fee { get; set; }
        public decimal Sub_fee_new { get; set; }
        public decimal Delivery_fee_discount { get; set; }
        public decimal Cod_fee_discount { get; set; }
        public int Shop_discount_config { get; set; }
        public decimal Shop_discount { get; set; }
        public decimal Sendo_discount { get; set; }
        public int Event_id { get; set; }
        public decimal DeliveryDuration { get; set; }
        public object DeliveryServices { get; set; }
    }
}