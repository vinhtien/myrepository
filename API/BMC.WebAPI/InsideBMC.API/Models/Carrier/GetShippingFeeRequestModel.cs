﻿namespace InsideBMC.API.Models.Carrier
{
    public class GetShippingFeeRequestModel
    {
        public int StoreId { get; set; }

        public int FromWardId { get; set; }
        public int FromDistrictId { get; set; }
        public int FromRegionId { get; set; }

        public int ToWardId { get; set; }
        public int ToDistrictId { get; set; }
        public int ToRegionId { get; set; }

        public decimal TotalWeight { get; set; }
        public decimal SubTotal { get; set; }

        public int CarrierId { get; set; }
    }
}