﻿using System;
using System.Collections.Generic;

namespace InsideBMC.API.Models.Email
{
    public class EmailModel
    {
        public EmailAddress From { get; set; }
        public List<EmailAddress> To { get; set; }
        public List<EmailAddress> Cc { get; set; }
        public List<EmailAddress> Bcc { get; set; }
        public string Subject { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public string Body { get; set; }
        public bool? IsInternal { get; set; }
        public int? EmailType { get; set; }  // 1:cs - 3: noi bo - 4: kinh doanh - 5: dich vu - 6: 123 mua
        public string SeenUID { get; set; }
        public List<MailAttachment> Attachments { get; set; }
        public string TemplateName { get; set; }
        public List<EmailParam> EmailParams { get; set; }
    }

    public class EmailAddress
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }

    public class MailAttachment
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }

    public class EmailParam
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class EmailResultData
    {
        public object Data { get; set; }
        public string Message { get; set; }
        public int StatusCode { get; set; }
    }
}