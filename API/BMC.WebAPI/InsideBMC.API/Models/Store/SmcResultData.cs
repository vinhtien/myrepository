﻿using System;

namespace InsideBMC.API.Models.Store
{
    public class SmcResultData
    {
        public bool success { get; set; }
        public object result { get; set; }
        public SmcError error { get; set; }
    }

    public class SmcError
    {
        public string message { get; set; }
        public string details { get; set; }
    }

    public class StoreModel
    {
        public int id { get; set; }
        public string userName { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string shopAddress { get; set; }
        public string shopWard { get; set; }
        public int shopDistrictId { get; set; }
        public int shopRegionId { get; set; }
        public int shopWardId { get; set; }
        public int warehouseWardId { get; set; }
        public string shopZipCode { get; set; }
        public string warehouseAddress { get; set; }
        public string warehouseWard { get; set; }
        public int warehouseDistrictId { get; set; }
        public int warehouseRegionId { get; set; }
        public string warehouseZipCode { get; set; }
        public string mobile { get; set; }
        public string website { get; set; }
        public string shopBackground { get; set; }
        public string shopCover { get; set; }
        public string shopLogo { get; set; }
        public string systemURL { get; set; }
        public string slogan { get; set; }
        public string shopDescription { get; set; }
        public string shopRepresentative { get; set; }
        public int shopStatus { get; set; }
        public string reasonComment { get; set; }
        public string email { get; set; }
        public DateTime lastLogin { get; set; }
        public string reasonCode { get; set; }
    }
}