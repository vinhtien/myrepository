﻿using System;
using System.Collections.Generic;

namespace InsideBMC.API.Models.Order
{
    public class OpcResultData
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public object Results { get; set; }
        public int TotalRecords { get; set; }
        public object OrderDetail { get; set; }
        public object ListProduct { get; set; }
        public object ListPayment { get; set; }
        public object ListStatusHistory { get; set; }
    }

    public class OrderDetail
    {
        public string Id { get; set; }
        public string OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public int OrderStatus { get; set; }
        public decimal TotalAmount { get; set; }
        public int BuyerId { get; set; }
        public string BuyerName { get; set; }
        public string BuyerPhone { get; set; }
        public string CustEmail { get; set; }
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public string StorePhone { get; set; }
        public string MerchantEmail { get; set; }
        public string CarrierName { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal ShippingFee { get; set; }
        public decimal SendoSupportFee { get; set; }
        public decimal SellerShippingFee { get; set; }
        
        public bool IsDisputing { get; set; }
        public int? Stage { get; set; }
        public int ShippingType { get; set; }
        public int DeliveryStatus { get; set; }
        public int PaymentStatus { get; set; }
        public int PaymentMethod { get; set; }
        public int? PaymentType { get; set; }
        public bool IsCODConfirm { get; set; }
        public string StorePickingAddress { get; set; }
        public string ReceiverName { get; set; }
        public string ShipToAddress { get; set; }
        public string ShipToWard { get; set; }
        public int? ShipToDistrictId { get; set; }
        public int? ShipToRegionId { get; set; }
        public string ShippingContactPhone { get; set; }

        public decimal Weight { get; set; }
        public decimal ActualWeight { get; set; }
        public string Note { get; set; }
        public decimal SubTotal { get; set; }
        public decimal CODFee { get; set; }
        public decimal BuyerFreeShipping { get; set; }

        public decimal LoyaltyAmount { get; set; }
        public decimal SellerLoyalAmount { get; set; }
        public int PartnerId { get; set; }
        public decimal MobileDiscountAmount { get; set; }
        public decimal SenpayFee { get; set; }
        public decimal InstallmentFee { get; set; }
        public bool IsExchangeOrder { get; set; }

        public int? CarrierId { get; set; }
        public decimal VoucherAmount { get; set; }
        public decimal SendoSupportFeeToBuyer { get; set; }

        public int? ShipToWardId { get; set; }
        public int? ShipFromWardId { get; set; }
        public int ShipFromDistrictId { get; set; }
        public int ShipFromRegionId { get; set; }
        public string ShipFromWardDisplay { get; set; }
        public string ShipFromDistrictDisplay { get; set; }
        public string ShipFromRegionDisplay { get; set; }
        public string ShipFromAddress { get; set; }
        public string ShipToDistrictDisplay { get; set; }
        public string ShipToRegionDisplay { get; set; }

        public bool IsUpdateActualWeight { get; set; }
        public decimal SenpayFreeShipping { get; set; }
    }

    public class ProductModel
    {
        public int? SalesOrderId { get; set; }
        public int? ProductVariantId { get; set; }
        public string ProductName { get; set; }
        public int? ColorId { get; set; }
        public string ColorValue { get; set; }
        public int? SizeId { get; set; }
        public string SizeValue { get; set; }
        public decimal? Weight { get; set; }
        public decimal? OriginalPrice { get; set; }
        public decimal? Price { get; set; }
        public int Quantity { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal? TaxAmount { get; set; }
        public decimal? TaxPercent { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? RowTotal { get; set; }
        public int? ExternalId { get; set; }
        public decimal? AffiliateAmount { get; set; }
        public string Description { get; set; }
        public string Sku { get; set; }
        public int? ProductStatus { get; set; }
        public string Ext_SalesOrderNumber { get; set; }
        public string Ext_Sku { get; set; }
        public List<string> Ext_LstAttOptional { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public int? Id { get; set; }
    }

    public class PaymentModel
    {
        public string PaymentNo { get; set; }
        public DateTime PaymentDate { get; set; }
        public int TransactionType { get; set; }
        public decimal Amount { get; set; }
        public string Note { get; set; }
        public int OrderId { get; set; }
        public int? PayeeId { get; set; }
        public string OrderNumber { get; set; }
        public DateTime? OrderDate { get; set; }
        public string Payee { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public int Id { get; set; }
    }

    public class StatusHistoryModel
    {
        public int OrderId { get; set; }
        public string OrderNumber { get; set; }
        public int OrderStatus { get; set; }
        public int PaymentStatus { get; set; }
        public string PaymentCode { get; set; }
        public bool IsOrderStatusUpdated { get; set; }
        public bool IsPaymentStatusUpdated { get; set; }
        public bool IsPaymentCodeUpdated { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public int Id { get; set; }
    }

    public class OrderModel
    {
        public string Id { get; set; }
        public string OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public int OrderStatus { get; set; }
        public decimal TotalAmount { get; set; }
        public int BuyerId { get; set; }
        public string BuyerName { get; set; }
        public string BuyerPhone { get; set; }
        public string BuyerAddress { get; set; }
        public string CustEmail { get; set; }
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public string StorePhone { get; set; }
        public string MerchantEmail { get; set; }
        public string CarrierName { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal ShippingFee { get; set; }
        public decimal SendoSupportFee { get; set; }
        public decimal SellerShippingFee { get; set; }

        public int PaymentMethod { get; set; }
        public int PaymentStatus { get; set; }
        public bool IsDisputing { get; set; }
    }

    public class ExchangeOrderModel
    {
        public string OrderNumber { get; set; }
        public int OrderParentId { get; set; }
        public int ExchangeType { get; set; }
        public decimal BuyerFee { get; set; }
        public decimal SellerFee { get; set; }
        public decimal SendoFee { get; set; }
        public decimal CODFee { get; set; }
        public decimal CODBuyerFee { get; set; }
        public decimal CODSellerFee { get; set; }
        public decimal CODSendoFee { get; set; }
        public string FromName { get; set; }
        public string FromPhone { get; set; }
        public string FromAddress { get; set; }
        public string ToName { get; set; }
        public string ToPhone { get; set; }
        public string ToAddress { get; set; }
        public int FromWardId { get; set; }
        public int FromDistrictId { get; set; }
        public int FromRegionId { get; set; }
        public int ToWardId { get; set; }
        public int ToDistrictId { get; set; }
        public int ToRegionId { get; set; }
        public string Note { get; set; }
        public int StoreId { get; set; }
        public string UserName { get; set; }
    }

    public class DeclareValueFeeModel
    {
        public string version { get; set; }
        public int status { get; set; }
        public string message { get; set; }
        public object result { get; set; }
        public bool flag { get; set; }
        public decimal data { get; set; }
    }

    public class CancelOrderForm
    {
        public string OrderNumber { get; set; }
        public DateTime CancelDate { get; set; }
        public string CancelReason { get; set; }
        public string CanceledBy { get; set; }
    }

    public class OrderSearchForm
    {
        public string OrderNumber { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string BuyerName { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

        public int OrderStatus { get; set; }
        public int? BuyerId { get; set; }
        public int PaymentMethod { get; set; }
        public int PaymentStatus { get; set; }
        public int? IsDisputing { get; set; }
        public string ReceiverName { get; set; }
        public string ShippingContactPhone { get; set; }
        public int CarrierId { get; set; }
        public int CarrierStatus { get; set; }
        public string ReasonCancelCode { get; set; }
    }
}