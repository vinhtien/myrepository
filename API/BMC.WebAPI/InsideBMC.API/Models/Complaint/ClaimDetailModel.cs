﻿using System;

namespace InsideBMC.API.Models.Complaint
{
    public class ClaimDetailModel
    {
        public int ClaimId { get; set; }
        public string ClaimNo { get; set; }
        public int ClaimStatus { get; set; }
        public DateTime ClaimStatusDate { get; set; }
        public int ClaimType { get; set; }
        public int Reason { get; set; }
        public string ReasonDetail { get; set; }
        public int OrderId { get; set; }
        public string OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal TotalPayment { get; set; }
        public int BuyerId { get; set; }
        public string BuyerName { get; set; }
        public string BuyerPhone { get; set; }
        public string BuyerEmail { get; set; }
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public string StorePhone { get; set; }
        public string StoreEmail { get; set; }
        public string CarrierName { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal ShippingFee { get; set; }
        public decimal SendoSupportFee { get; set; }
        public decimal SellerShippingFee { get; set; }

        public string BuyerNote { get; set; }
        public string SellerNote { get; set; }
        public string CSNote { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public string UpdatedDate { get; set; }

        //Thông tin bằng chứng của khiếu nại
        public int ClaimEvidenceId { get; set; }
        public string DocumentUrl { get; set; }
        public string DocumentName { get; set; }
        public int DisplayOrder { get; set; }

        //Thông tin phán quyết của khiếu nại
        public int JudgementId { get; set; }
        public int JudgementStatus { get; set; }
        public int JudgementType { get; set; }
        public string JudgementContent { get; set; }
        public decimal BuyerRefundAmount { get; set; }
        public decimal SellerRefundAmount { get; set; }
        public bool IsSellerViolation { get; set; }
    }
}