﻿using System;

namespace InsideBMC.API.Models.Complaint
{
    public class CommentModel
    {
        public int ClaimId { get; set; }
        public string Comment { get; set; }
        public string CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}