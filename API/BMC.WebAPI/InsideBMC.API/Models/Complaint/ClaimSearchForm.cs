﻿using System;

namespace InsideBMC.API.Models.Complaint
{
    public class ClaimSearchForm
    {
        public int ClaimId { get; set; }
        public string ClaimNo { get; set; }
        public string BuyerName { get; set; }
        public DateTime? ClaimCreatedDateFrom { get; set; }
        public DateTime? ClaimCreatedDateTo { get; set; }
        public int ClaimStatus { get; set; }
        public DateTime? ClaimUpdatedDateFrom { get; set; }
        public DateTime? ClaimUpdatedDateTo { get; set; }
        public string UpdatedUser { get; set; }
        public string OrderNumber { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
    }
}