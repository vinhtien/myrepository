﻿namespace InsideBMC.API.Models.Complaint
{
    public class ClaimSearchModel
    {
        public int ClaimId { get; set; }
        public string ClaimNo { get; set; }
        public int ClaimStatus { get; set; }
        public string OrderNumber { get; set; }
        public string BuyerName { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public string UpdatedDate { get; set; }
    }
}