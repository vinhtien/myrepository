﻿namespace InsideBMC.API.Models
{
    public class UploadImageResult
    {
        public string DocumentUrl { get; set; }
        public string DocumentName { get; set; }

        public bool status { get; set; }
        public string error { get; set; }
        public string filePath { get; set; }
    }
}