﻿namespace InsideBMC.API.Models
{
    public class ResultData
    {
        public ResultData(bool isError = true, string message = "", int totalCount = 0, object data = null)
        {
            this.IsError = isError;
            this.Message = message;
            this.TotalCount = totalCount;
            this.Data = data;
        }
        public bool IsError { get; set; }
        public string Message { get; set; }
        public int TotalCount { get; set; }
        public object Data { get; set; }
    }
}