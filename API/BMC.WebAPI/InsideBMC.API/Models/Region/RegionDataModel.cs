﻿using System.Collections.Generic;

namespace InsideBMC.API.Models.Region
{
    public class RegionDataModel
    {
        public List<RegionModel> ListRegion { get; set; }
        public List<DistrictModel> ListDistrict { get; set; }
        public List<WardModel> ListWard { get; set; }
    }

    public class RegionModel
    {
        public int RegionId { get; set; }
        public string RegionName { get; set; }
    }

    public class DistrictModel
    {
        public int RegionId { get; set; }
        public string RegionName { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
    }

    public class WardModel
    {
        public int RegionId { get; set; }
        public string RegionName { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int WardId { get; set; }
        public string WardName { get; set; }
    }

}