﻿using InsideBMC.API.Models;
using InsideBMC.API.Models.Order;
using InsideBMC.API.Services;
using System.Threading.Tasks;
using System.Web.Http;

namespace InsideBMC.API.Controllers
{
    public class OrderController : ApiController
    {
        public OrderService service = new OrderService();

        [HttpGet]
        public ResultData GetOrderDetail(int id)
        {
            return service.GetOrderDetail(id);
        }

        [HttpGet]
        [Route("Complaint/CalculateDeclareValueFee/{carrierCode}/{subTotal}/{shippingFee}/{totalAmount}")]
        public async Task<ResultData> CalculateDeclareValueFee(string carrierCode, decimal subTotal, decimal shippingFee, decimal totalAmount)
        {
            return await service.CalculateDeclareValueFee(carrierCode, subTotal, shippingFee, totalAmount);
        }

        [HttpGet]
        [Route("Complaint/GetExchangeOrderNumber/{orderParentNumber}/{orderParentId}/{exchangeType}")]
        public ResultData GetExchangeOrderNumber(string orderParentNumber, int orderParentId, int exchangeType)
        {
            return service.GetExchangeOrderNumber(orderParentNumber, orderParentId, exchangeType);
        }

        [HttpPost]
        public ResultData CreateExchangeOrder(ExchangeOrderModel exchangeOrderModel)
        {
            //User.Identity.Name
            return service.CreateExchangeOrder(exchangeOrderModel);
        }

        [HttpPost]
        public ResultData SearchOrder(OrderSearchForm orderSearchForm)
        {
            return service.SearchOrder(orderSearchForm);
        }

        [HttpGet]
        public ResultData GetListPaymentByOrderId(int id)
        {
            return service.GetListPaymentByOrderId(id);
        }

        [HttpGet]
        public ResultData GetListStatusHistoryByOrderId(int id)
        {
            return service.GetListStatusHistoryByOrderId(id);
        }

        [HttpGet]
        public ResultData GetExchangeOrderByOrderId(int id)
        {
            return service.GetExchangeOrderByOrderId(id);
        }

        [HttpPost]
        public ResultData CancelOrder(CancelOrderForm cancelOrderForm)
        {
            return service.CancelOrder(cancelOrderForm);
        }

        [HttpPost]
        public ResultData GetListOrderByBuyerId(OrderSearchForm orderSearchForm)
        {
            return service.GetListOrderByBuyerId(orderSearchForm);
        }
    }
}
