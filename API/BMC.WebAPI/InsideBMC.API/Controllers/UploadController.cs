﻿using InsideBMC.API.Models;
using InsideBMC.API.Services;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace InsideBMC.API.Controllers
{
    public class UploadController : ApiController
    {
        public UploadService uploadService = new UploadService();

        [HttpPost]
        public ResultData UploadImage()
        {
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var httpPostedFile = HttpContext.Current.Request.Files["UploadedImage"];

                if (httpPostedFile != null)
                {
                    //return complaintService.UploadImage(httpPostedFile);
                    return uploadService.UploadImageTest(httpPostedFile);
                }
            }
            return new ResultData(isError: true, message: "Tại API không thấy file!");
        }

        [HttpPost]
        public async Task<ResultData> UploadImageToAmazon()
        {
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var httpPostedFile = HttpContext.Current.Request.Files["UploadedImage"];

                if (httpPostedFile != null)
                {
                    return await uploadService.UploadImageToAmazon(httpPostedFile);
                }
            }
            return new ResultData(isError: true, message: "Tại API không thấy file!");
        }
    }
}
