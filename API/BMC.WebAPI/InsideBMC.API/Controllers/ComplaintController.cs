﻿using InsideBMC.API.Models;
using InsideBMC.API.Models.Complaint;
using InsideBMC.API.Models.Order;
using InsideBMC.API.Services;
using System.Web.Http;

namespace InsideBMC.API.Controllers
{
    public class ComplaintController : ApiController
    {
        public ComplaintService complaintService = new ComplaintService();

        #region INSIDE

        [HttpPost]
        public ResultData SearchClaim(ClaimSearchForm claimSearchForm)
        {
            return complaintService.SearchClaim(claimSearchForm);
        }

        [HttpGet]
        public ResultData GetClaimById(int id)
        {
            return complaintService.GetClaimById(id);
        }

        [HttpGet]
        public ResultData GetNextClaim()
        {
            return complaintService.GetNextClaim();
        }

        [HttpPost]
        public ResultData CreateClaim(ClaimModel claimModel)
        {
            //User.Identity.Name
            return complaintService.CreateClaim(claimModel, "tienhv");
        }

        [HttpPost]
        public ResultData UpdateClaim(ClaimModel claimModel)
        {
            return complaintService.UpdateClaim(claimModel, "tienhv");
        }

        [HttpPost]
        public ResultData UpdateClaimStatus(ClaimModel claimModel)
        {
            return complaintService.UpdateClaimStatus(claimModel, "tienhv");
        }

        [HttpPost]
        public ResultData AcceptClaim(ClaimModel claimModel)
        {
            claimModel.ClaimStatus = (int)ClaimStatus.CS_REQUEST_NEGOTIATE;
            return complaintService.UpdateClaimStatus(claimModel, "cs");
        }

        [HttpPost]
        public ResultData RequestUpdateEvidence(ClaimModel claimModel)
        {
            claimModel.ClaimStatus = (int)ClaimStatus.CS_REQUEST_UPDATE;
            return complaintService.UpdateClaimStatus(claimModel, "cs");
        }

        [HttpPost]
        public ResultData CancelClaim(ClaimModel claimModel)
        {
            claimModel.ClaimStatus = (int)ClaimStatus.CANCEL;
            return complaintService.UpdateClaimStatus(claimModel, "cs");
        }

        [HttpPost]
        public ResultData CloseClaim(ClaimModel claimModel)
        {
            claimModel.ClaimStatus = (int)ClaimStatus.CLOSED;
            return complaintService.UpdateClaimStatus(claimModel, "cs");
        }

        [HttpPost]
        public ResultData InsertJudgement(ClaimModel claimModel)
        {
            return complaintService.InsertJudgement(claimModel, "cs");
        }

        [HttpPost]
        public ResultData UpdateJudgement(ClaimModel claimModel)
        {
            ResultData result = complaintService.UpdateJudgement(claimModel, "cs");
            if (result.IsError == false)
            {
                claimModel.ClaimStatus = (int)ClaimStatus.CS_MAKE_DICISION;
                return complaintService.UpdateClaimStatus(claimModel, "cs");
            }
            return result;
        }

        [HttpPost]
        public ResultData ApplyJudgement(ClaimModel claimModel)
        {
            ResultData result = complaintService.UpdateJudgement(claimModel, "cs");
            if (result.IsError == false)
            {
                claimModel.ClaimStatus = (int)ClaimStatus.CLOSED;
                return complaintService.UpdateClaimStatus(claimModel, "cs");
            }
            return result;
        }

        [HttpPost]
        public ResultData SearchOrderForComplaint(OrderSearchForm orderSearchForm)
        {
            return complaintService.SearchOrderForComplaint(orderSearchForm);
        }

        [HttpPost]
        public ResultData InsertComment(ClaimModel claimModel)
        {
            return complaintService.InsertComment(claimModel, "CS");
        }

        [HttpGet]
        public ResultData GetClaimComment(int id)
        {
            return complaintService.GetClaimComment(id);
        }

        [HttpGet]
        public ResultData GetClaimListByBuyerId(int id, int pageNo, int pageSize)
        {
            return complaintService.GetClaimListByBuyerId(id, pageNo, pageSize);
        }

        #endregion

        #region BUYER

        //01 Buyer API
        [HttpGet]
        public ResultData GetProductListByOrderId(int id)
        {
            return complaintService.GetProductListByOrderId(id);
        }

        //02 Buyer API
        [HttpPost]
        public ResultData CreateClaimFromBuyer(ClaimModel claimModel)
        {
            return complaintService.CreateClaimFromBuyer(claimModel, "Buyer");
        }

        //03 Buyer API
        [HttpGet]
        public ResultData GetClaimByOrderId(int id)
        {
            return complaintService.GetClaimByOrderId(id);
        }

        //04 Buyer API
        [HttpGet]
        public ResultData GetClaimListByBuyerId_TabCompleted(int id)
        {
            return complaintService.GetClaimListByBuyerId_TabCompleted(id);
        }

        //05 Buyer API
        [HttpGet]
        public ResultData GetOrderListByBuyerId_POD_48H(int id)
        {
            return complaintService.GetOrderListByBuyerId_POD_48H(id);
        }

        //06 Buyer API
        [HttpGet]
        public ResultData GetClaimListByBuyerId_TabProcessing(int id)
        {
            return complaintService.GetClaimListByBuyerId_TabProcessing(id);
        }

        //07 Buyer API
        [HttpPost]
        public ResultData UpdateClaimEvidences(ClaimModel claimModel)
        {
            return complaintService.UpdateClaimEvidences(claimModel, "tienhv");
        }

        //08 Buyer API
        [HttpPost]
        public ResultData BuyerDenyNegotiate(ClaimModel claimModel)
        {
            claimModel.ClaimStatus = (int)ClaimStatus.BUYER_DENY_NEGOTIATE;
            return complaintService.UpdateClaimStatus(claimModel, "buyer");
        }

        //09 Buyer API
        [HttpPost]
        public ResultData BuyerAcceptNegotiate(ClaimModel claimModel)
        {
            claimModel.ClaimStatus = (int)ClaimStatus.BUYER_ACCEPT_NEGOTIATE;
            return complaintService.UpdateClaimStatus(claimModel, "buyer");
        }

        //10 Buyer API
        [HttpPost]
        public ResultData BuyerUpdateClaimStatus(ClaimModel claimModel)
        {
            return complaintService.UpdateClaimStatus(claimModel, "buyer");
        }

        #endregion

        #region SELLER

        //01 Seller API
        [HttpPost]
        public ResultData SellerDenyNegotiate(ClaimModel claimModel)
        {
            claimModel.ClaimStatus = (int)ClaimStatus.SELLER_DENY_NEGOTIATE;
            return complaintService.UpdateClaimStatus(claimModel, "seller");
        }

        //02 Seller API
        [HttpPost]
        public ResultData SellerAcceptNegotiate(ClaimModel claimModel)
        {
            claimModel.ClaimStatus = (int)ClaimStatus.SELLER_ACCEPT_NEGOTIATE;
            return complaintService.UpdateClaimStatus(claimModel, "seller");
        }

        #endregion

        #region OPC

        [HttpGet]
        public ResultData OpcGetClaimByOrderId(int id)
        {
            return complaintService.OpcGetClaimByOrderId(id);
        }

        #endregion
    }
}
