﻿using InsideBMC.API.Models;
using InsideBMC.API.Models.Carrier;
using InsideBMC.API.Services;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace InsideBMC.API.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class CarrierController : ApiController
    {
        public CarrierService service = new CarrierService();

        [HttpGet]
        [Route("Carrier/GetShippingFee/{storeId}/{fromRegionId}/{toRegionId}/{fromDistrictId}/{toDistrictId}/{fromWardId}/{toWardId}/{actualWeight}/{subTotal}/{carrierId}")]
        public async Task<ResultData> GetShippingFee(int storeId,
                                                     int fromRegionId,
                                                     int toRegionId,
                                                     int fromDistrictId,
                                                     int toDistrictId,
                                                     int fromWardId,
                                                     int toWardId,
                                                     decimal actualWeight,
                                                     decimal subTotal,
                                                     int carrierId)
        {
            return await service.GetShippingFee(storeId, fromRegionId, toRegionId, fromDistrictId, toDistrictId, fromWardId, toWardId, actualWeight, subTotal, carrierId);
        }

        [HttpGet]
        public async Task<ResultData> GetCarriers()
        {
            return await service.GetCarriers();
        }
    }
}
