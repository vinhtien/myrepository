﻿using InsideBMC.API.Models;
using InsideBMC.API.Models.Email;
using InsideBMC.API.Services;
using System.Threading.Tasks;
using System.Web.Http;

namespace InsideBMC.API.Controllers
{
    public class EmailController : ApiController
    {
        [HttpPost]
        public async Task<ResultData> SendEmail(EmailModel model)
        {
            return await EmailService.Send(model);
        }
    }
}
