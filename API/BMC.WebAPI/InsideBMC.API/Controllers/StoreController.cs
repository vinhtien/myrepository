﻿using InsideBMC.API.Models;
using InsideBMC.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace InsideBMC.API.Controllers
{
    public class StoreController : ApiController
    {
        public StoreService service = new StoreService();

        [HttpGet]
        public async Task<ResultData> GetStore(int id)
        {
            return await service.GetStore(id);
        }
    }
}
