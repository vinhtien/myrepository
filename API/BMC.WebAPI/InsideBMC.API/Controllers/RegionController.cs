﻿using InsideBMC.API.Models;
using InsideBMC.API.Services;
using System.Web.Http;

namespace InsideBMC.API.Controllers
{
    public class RegionController : ApiController
    {
        public RegionService service = new RegionService();

        [HttpGet]
        public ResultData GetRegion()
        {
            return service.GetRegion();
        }

        [HttpGet]
        public ResultData GetDistrict()
        {
            return service.GetDistrict();
        }

        [HttpGet]
        public ResultData GetDistrictByRegion(int id)
        {
            return service.GetDistrictByRegion(id);
        }

        [HttpGet]
        public ResultData GetWardByDistrict(int id)
        {
            return service.GetWardByDistrict(id);
        }

        [HttpGet]
        [Route("Region/GetRegionData/{regionId}/{districtId}")]
        public ResultData GetRegionData(int regionId, int districtId)
        {
            return service.GetRegionData(regionId, districtId);
        }
    }
}
