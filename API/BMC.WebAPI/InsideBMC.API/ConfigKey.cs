﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InsideBMC.API
{
    public static class ConfigKey
    {
        public const string CONNECTION_STRING_NAME = "ConnectionStringName";

        public const string IMAGE_EXTENSION_ALLOW = "ImageExtensionAllow";

        public const string IMAGE_UPLOAD_SIZE = "ImageUploadSize";

        public const string UPLOAD_SERVER_DOMAIN = "UploadServerDomain";

        public const string API_UPLOAD_DOMAIN = "ApiUploadDomain";
    }
}