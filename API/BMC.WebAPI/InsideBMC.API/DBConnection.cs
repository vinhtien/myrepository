﻿using Dapper;
using InsideBMC.API.Models.Region;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using static Dapper.SqlMapper;

namespace InsideBMC.API
{
    public static class DBConnection
    {
        /// <summary>
        /// Get chuỗi kết nối DB thiết lập tại web.config
        /// </summary>
        public static string connectionString = ConfigurationManager.ConnectionStrings[ConfigKey.CONNECTION_STRING_NAME].ConnectionString;

        /// <summary>
        /// QueryByStoreProcedure
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storeProcedureName">tên store procedure</param>
        /// <param name="errorMessage">message thông báo lỗi</param>
        /// <param name="parameters">tham số truyền vào store procedure</param>
        /// <returns></returns>
        static public List<T> QueryByStoreProcedure<T>(string storeProcedureName, out string errorMessage, object parameters = null)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(connectionString))
                {
                    db.Open();
                    DynamicParameters param = (DynamicParameters)parameters;
                    List<T> result = db.Query<T>(storeProcedureName, param, commandType: CommandType.StoredProcedure).ToList();
                    
                    errorMessage = param.Get<string>("@PO_ErrorMessage");
                    return result;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return null;
            }
        }

        /// <summary>
        /// QueryByStoreProcedurePaging
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storeProcedureName">tên store procedure</param>
        /// <param name="errorMessage">message thông báo lỗi</param>
        /// <param name="totalCount">tổng record</param>
        /// <param name="parameters">tham số truyền vào store procedure</param>
        /// <returns></returns>
        static public List<T> QueryByStoreProcedurePaging<T>(string storeProcedureName, out string errorMessage, out int totalCount, object parameters = null)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(connectionString))
                {
                    db.Open();
                    DynamicParameters param = (DynamicParameters)parameters;
                    List<T> result = db.Query<T>(storeProcedureName, param, commandType: CommandType.StoredProcedure).ToList();

                    totalCount = param.Get<int>("@PO_TotalCount");
                    errorMessage = param.Get<string>("@PO_ErrorMessage");
                    return result;
                }
            }
            catch (Exception ex)
            {
                totalCount = 0;
                errorMessage = ex.Message;
                return null;
            }
        }

        static public RegionDataModel QueryRegionData(string storeProcedureName, out string errorMessage, object parameters = null)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(connectionString))
                {
                    db.Open();
                    DynamicParameters param = (DynamicParameters)parameters;
                    var result = db.QueryMultiple(storeProcedureName, param, commandType: CommandType.StoredProcedure);
                    errorMessage = param.Get<string>("@PO_ErrorMessage");

                    RegionDataModel dataModel = new RegionDataModel();
                    dataModel.ListRegion = result.Read<RegionModel>().ToList();
                    dataModel.ListDistrict = result.Read<DistrictModel>().ToList();
                    dataModel.ListWard = result.Read<WardModel>().ToList();
                    return dataModel;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return null;
            }
        }

        /// <summary>
        /// Dùng để thực thi store insert/update/delete
        /// </summary>
        /// <param name="storeProcedureName">tên store procedure</param>
        /// <param name="errorMessage">message thông báo lỗi</param>
        /// <param name="parameters">tham số truyền vào store procedure</param>
        /// <returns></returns>
        static public int ExecuteCommandByStoreProcedure(string storeProcedureName,out string errorMessage, object parameters = null)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(connectionString))
                {
                    db.Open();
                    DynamicParameters param = (DynamicParameters)parameters;
                    int result = db.Execute(storeProcedureName, param, commandType: CommandType.StoredProcedure);

                    if (result == 0)
                    {
                        errorMessage = "Thực hiện store procedure không lỗi, nhưng không dòng nào được áp dụng.";
                    }
                    else
                    {
                        errorMessage = param.Get<string>("@PO_ErrorMessage");
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                return -1;
            }
        }

        /// <summary>
        /// Dùng để thực thi store insert/update/delete có return id
        /// </summary>
        /// <param name="storeProcedureName">tên store procedure</param>
        /// <param name="errorMessage">message thông báo lỗi</param>
        /// <param name="id">Id của câu insert</param>
        /// <param name="parameters">tham số truyền vào store procedure</param>
        /// <returns></returns>
        static public int ExecuteStoreProcedureReturnId(string storeProcedureName, out string errorMessage, out int id, object parameters = null)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(connectionString))
                {
                    db.Open();
                    DynamicParameters param = (DynamicParameters)parameters;
                    int result = db.Execute(storeProcedureName, param, commandType: CommandType.StoredProcedure);

                    if (result == 0)
                    {
                        errorMessage = "Thực hiện store procedure không lỗi, nhưng không dòng nào được áp dụng.";
                        id = 0;
                    }
                    else
                    {
                        errorMessage = param.Get<string>("@PO_ErrorMessage");
                        id = param.Get<int>("@PO_Id");
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                id = 0;
                return -1;
            }
        }

        ///// <summary>
        ///// ExecuteQuery
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="sql">string sql</param>
        ///// <param name="parameters">tham số truyền vào sql</param>
        ///// <returns></returns>
        //static public List<T> ExecuteQuery<T>(string sql, object parameters = null)
        //{
        //    using (IDbConnection db = new SqlConnection(connectionString))
        //    {
        //        db.Open();
        //        return db.Query<T>(sql, parameters).ToList();
        //    }
        //}


    }
}